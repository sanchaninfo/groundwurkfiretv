/*
     Module Name :
     File Name   :      downClickInDescription.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0 
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      navigation of buttons in description page for down arrow click
     Organisation:      Peafowl inc.
*/
function descriptionDownlClick(episodeButtonVisible, resumeButtonVisible, stat) {
var activeClassCount =0;
    if (episodeButtonVisible && stat == 0 && resumeButtonVisible!=true) {
        if (flagButtonMovement == 0) {
            if (episodeClick < 3) {


                if (episodeClick == 1) {
                    $("#episode_action" + episodeClick).removeClass("ps1_landingPage_active");
                    $("#episode_action" + episodeClick).css({
                        color: 'white'
                    });
                }

                if (episodeClick == 2) {
                    if (flagEnterVariable == 1) {
                        $("#episode_action" + episodeClick).removeClass("addlist_active1");
                        $("#episode_action" + episodeClick).css({
                            color: 'white'
                        });
                        $("#episode_action" + episodeClick).removeClass("addlist_active");
                        $("#episode_action" + episodeClick).css({
                            color: 'white'
                        });
                        $("#episode_action" + episodeClick).addClass("addlist1");
                        $("#episode_action" + episodeClick).css({
                            color: 'white'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action" + episodeClick).removeClass("addlist_active");
                                $("#episode_action" + episodeClick).css({
                                    color: 'white'
                                });
                            } else {
                                $("#episode_action" + episodeClick).removeClass("addlist_active1");
                                $("#episode_action" + episodeClick).css({
                                    color: 'white'
                                });
                            }

                        } else {
                            $("#episode_action" + episodeClick).removeClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        }

                    }
                }


                episodeClick++;
                if (episodeClick == 3) {
                    $("#episode_action"+episodeClick).removeClass("rating");
                    $("#episode_action" + episodeClick).addClass("star_active1");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                   // $("#episode_action" + episodeClick).addClass("star_active");
                    
                    // $("#episode_action" + episodeClick).html("");
                  // $("#episode_action" + episodeClick).removeClass("rating");
                   // alert("#episode_action" + episodeClick)
                     //$("#episode_action" + episodeClick).removeClass("rating");
                   // $("#episode_action" + episodeClick).addClass("rating_active");
                    // $("#episode_action" + episodeClick).html("");
                    var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCount){
                            activeClassCount++;
                             userRatingHtml = userRatingHtml + '<div id = "rating'+(assetRating+1)+'" class="star_active"></div>';
                            
                        }
                        else{
                           
                            userRatingHtml = userRatingHtml + '<div id = "rating'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                  /*   $("#episode_action" + episodeClick).html('<div id="starRateMainId" class="starRateMain"><div id = "rating1" class="star"></div><div id = "rating2" class="star"></div><div id = "rating3" class="star"></div><div id = "rating4" class="star"></div><div id = "rating5" class="star"></div></div>');*/

                  $("#episode_action" + episodeClick).html('<div id="starRateMainId" class="starRateMain">'+userRatingHtml+'</div>');
                }
                if (episodeClick == 2) {
                    if (flagEnterVariable == 1) {
                        $("#episode_action" + episodeClick).addClass("addlist_active1");
                        $("#episode_action" + episodeClick).css({
                            color: 'black'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action" + episodeClick).addClass("addlist_active");
                                $("#episode_action" + episodeClick).css({
                                    color: 'black'
                                });
                            } else {
                                $("#episode_action" + episodeClick).addClass("addlist_active1");
                                $("#episode_action" + episodeClick).css({
                                    color: 'black'
                                });
                            }

                        } else {

                            $("#episode_action" + episodeClick).addClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        }
                    }

                }

               
            }
        }

    } else if (resumeButtonVisible && stat == 0) {
        if (flagButtonMovement == 0) {
            if (episodeClick < 4) {
                episodeClick++;
                if(episodeClick == 4){
                       $("#episode_action"+(episodeClick-1)).removeClass("rating");
                    $("#episode_action" + (episodeClick-1)).addClass("star_active1");
                    $("#episode_action" + (episodeClick-1)).css({
                        color: 'black'
                    });
                     $("#episode_action" + (episodeClick-1)).html("");
                    var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(userratingsum>activeClassCount){
                            activeClassCount++;
                             userRatingHtml = userRatingHtml + '<div id = "rating'+(assetRating+1)+'" class="star_active"></div>';
                            
                        }
                        else{
                           
                            userRatingHtml = userRatingHtml + '<div id = "rating'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }

                  $("#episode_action" + (episodeClick-1)).html('<div id="starRateMainId" class="starRateMain">'+userRatingHtml+'</div>');
                }
                if (episodeClick == 3) {
                    if (flagEnterVariable == 1) {
                        $("#episode_action2").addClass("addlist_active1");
                        $("#episode_action2").css({
                            color: 'black'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action2").addClass("addlist_active");
                                $("#episode_action2").css({
                                    color: 'black'
                                });
                            } else {
                                $("#episode_action2").addClass("addlist_active1");
                                $("#episode_action2").css({
                                    color: 'black'
                                });
                            }

                        } else {

                            $("#episode_action2").addClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'black'
                            });
                        }
                    }
                }

                if (episodeClick == 2) {

                    $("#episode_action1").addClass("ps1_landingPage_active");
                    $("#episode_action1").css({
                        color: 'black'
                    });

                }

                episodeClick--;

                if (episodeClick == 1) {
                    $("#episode_actionResume").removeClass("ps1_landingPageResume_active");
                    $("#episode_actionResume").css({
                        color: 'white'
                    });
                }

                if (episodeClick == 2) {
                    $("#episode_action1").removeClass("ps1_landingPage_active");
                    $("#episode_action1").css({
                        color: 'white'
                    });
                }
                  if(episodeClick == 3){
                     if (flagEnterVariable == 1) {
                        $("#episode_action2").removeClass("addlist_active1");
                        $("#episode_action2").css({
                            color: 'white'
                        });
                        $("#episode_action2").removeClass("addlist_active");
                        $("#episode_action2").css({
                            color: 'white'
                        });
                        $("#episode_action2").addClass("addlist1");
                        $("#episode_action2").css({
                            color: 'white'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action2").removeClass("addlist_active");
                                $("#episode_action2").css({
                                    color: 'white'
                                });
                            } else {
                                $("#episode_action2").removeClass("addlist_active1");
                                $("#episode_action2").css({
                                    color: 'white'
                                });
                            }

                        } else {
                            $("#episode_action2").removeClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        }

                    }

                  }

                episodeClick++;
            }
        }
    } else if ((episodeButtonVisible == true && resumeButtonVisible == true) && stat == 1) {
        if (flagButtonMovement == 0) {
            if (episodeClick < 5) {
                episodeClick++;
                if(episodeClick == 5){
                       $("#episode_action"+episodeClick).removeClass("rating");
                    $("#episode_action" + episodeClick).addClass("star_active1");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                     $("#episode_action" + episodeClick).html("");
                    var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(userratingsum>activeClassCount){
                            activeClassCount++;
                             userRatingHtml = userRatingHtml + '<div id = "rating'+(assetRating+1)+'" class="star_active"></div>';
                            
                        }
                        else{
                           
                            userRatingHtml = userRatingHtml + '<div id = "rating'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }

                  $("#episode_action" + episodeClick).html('<div id="starRateMainId" class="starRateMain">'+userRatingHtml+'</div>');
                }
                if (episodeClick == 4) {
                    //$("#episode_action"+episodeClick).removeClass("episodes");
                    $("#episode_action3").addClass("episode_active1");
                    $("#episode_action3").css({
                        color: 'black'
                    });
                }
                if (episodeClick == 3) {
                    if (flagEnterVariable == 1) {
                        $("#episode_action2").addClass("addlist_active1");
                        $("#episode_action2").css({
                            color: 'black'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action2").addClass("addlist_active");
                                $("#episode_action2").css({
                                    color: 'black'
                                });
                            } else {
                                $("#episode_action2").addClass("addlist_active1");
                                $("#episode_action2").css({
                                    color: 'black'
                                });
                            }

                        } else {

                            $("#episode_action2").addClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'black'
                            });
                        }
                    }
                }
                if (episodeClick == 2) {
                    $("#episode_action1").addClass("ps1_landingPage_active");
                    $("#episode_action1").css({
                        color: 'black'
                    });

                }

                episodeClick--;

                if (episodeClick == 1) {
                    $("#episode_actionResume").removeClass("ps1_landingPageResume_active");
                    $("#episode_actionResume").css({
                        color: 'white'
                    });
                }

                if (episodeClick == 2) {
                    $("#episode_action1").removeClass("ps1_landingPage_active");
                    $("#episode_action1").css({
                        color: 'white'
                    });
                }
                if (episodeClick == 3) {
                    if (flagEnterVariable == 1) {
                        $("#episode_action2").removeClass("addlist_active1");
                        $("#episode_action2").css({
                            color: 'white'
                        });
                        $("#episode_action2").removeClass("addlist_active");
                        $("#episode_action2").css({
                            color: 'white'
                        });
                        $("#episode_action2").addClass("addlist1");
                        $("#episode_action2").css({
                            color: 'white'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action2").removeClass("addlist_active");
                                $("#episode_action2").css({
                                    color: 'white'
                                });
                            } else {
                                $("#episode_action2").removeClass("addlist_active1");
                                $("#episode_action2").css({
                                    color: 'white'
                                });
                            }

                        } else {
                            $("#episode_action2").removeClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        }

                    }
                }
                if(episodeClick == 4){
                     $("#episode_action3").removeClass("episode_active1");
                    $("#episode_action3").css({
                        color: 'white'
                    });
                }

                episodeClick++;
            }
        }
    } else {

        if (episodeClick < 2) {
            episodeClick++;


            if (episodeClick == 2) {
                if (flagEnterVariable == 1) {
                    $("#episode_action" + episodeClick).addClass("addlist_active1");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            $("#episode_action" + episodeClick).addClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        } else {
                            $("#episode_action" + episodeClick).addClass("addlist_active1");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        }

                    } else {

                        $("#episode_action" + episodeClick).addClass("addlist_active");
                        $("#episode_action" + episodeClick).css({
                            color: 'black'
                        });
                    }
                }

            }

            episodeClick--;

            if (episodeClick == 1) {
                $("#episode_action" + episodeClick).removeClass("ps1_landingPage_active");
                $("#episode_action" + episodeClick).css({
                    color: 'white'
                });
            }


            episodeClick++;
        }
    }
}