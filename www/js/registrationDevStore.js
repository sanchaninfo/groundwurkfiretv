/**
 *
  Module Name :      
  File Name   :      registration.js
  Project     :      Store
  Copyright (c)      peafowl inc.
  author      :      Siddartha
  author      :      
  license     :   
  version     :      0.1.0  
  Created on  :      17 July 12:20 pm
  Last modified on:  28 jan 2017
  Description :      This file contains javascript functionality for pairing of device i.e for getting activation code 
                     and storing the user details in local storage.  
  Organisation:      Peafowl inc.
 */

function createDataBase() {
    var storage = window.localStorage;
    storage.setItem("keyModel", device.model);
    storage.setItem("keyManufacturer", device.manufacturer);
    storage.setItem("keyDeviceId", device.uuid);
    storage.setItem("keyDeviceMacAddress", "");
    storage.setItem("keyBrandName", "amazon");
    storage.setItem("keyHostName", "amazon");
    storage.setItem("keyDisplayName", "amazon");
    storage.setItem("keySerialNumber", device.serial);
    var value = storage.getItem("keyDisplayName");
    var myKeyVals = { "getActivationCode": { "model": storage.getItem("keyModel"), "manufacturer": storage.getItem("keyManufacturer"), "device_name": "amazon", "device_id": storage.getItem("keyDeviceId"), "device_mac_address": storage.getItem("keyDeviceId"), "brand_name": "Amazon", "host_name": "ip-10-11-3-68", "display_name": "amazon", "serial_number": storage.getItem("keySerialNumber") } }

    $.ajax({
        type: "POST",
        url: mainUrl + getActivation,
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(myKeyVals),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          
            storage.setItem("codeValue", data.code);
            storage.setItem("keyUUIDValue", data.uuid);
            storage.setItem("_idValue", data._id);
            storage.setItem("pairStatus", data.pair_devce);
        },
        error: function (jqXHR, textStatus, errorThrown) {

        },
        complete: function () {

            window.location.href = 'registration.html';
        }
    });
}

 function getRegisteredData(isInRegistrationHTML) {

     var storage = window.localStorage;
     var myKeyVals = { "getAccountInfo": { "deviceId": storage.getItem("keyDeviceId"), "uuid": storage.getItem("keyUUIDValue") } }

     $.ajax({
         type: "POST",
         url: mainUrl + getAccountInfo,
         data: JSON.stringify(myKeyVals),
         contentType: "application/json; charset=utf-8",
         dataType: "json",
         success: function (data) {
         
             storage.setItem("userId", data._id);
             storage.setItem("email", data.email);
            
             storage.setItem("fullName", data.full_name);
             storage.setItem("user_status", data.user_status);
             storage.setItem("user_id", data.user_id);
             storage.setItem("customer_id", data.customer_id);
             storage.setItem("store_uuidExist", data.uuid_exist);
             //alert("userid"+data._id);
         },
         error: function (jqXHR, textStatus, errorThrown) {

         },
         complete: function () {
 
             if (storage.getItem("store_uuidExist") == "true") {
                 if (isInRegistrationHTML == true && storage.getItem("user_status") == 'active') {
                   
                     window.location.href = 'index.html';
                    
                 }
                 else if (isInRegistrationHTML == false) {
                     storingIn = storage.getItem("userId");
                      getassetdata();
                     launchCheckUUIDExistFlag = 0;
                 }
             } else {
                 if (isInRegistrationHTML == true) {
                 }
                 else {
                     storage.removeItem("codeValue");
                     storage.removeItem("keyUUIDValue");
                     storage.removeItem("_idValue");
                     storage.removeItem("pairStatus");
                     storage.removeItem("userId");

                     storage.removeItem("email");

                     storage.removeItem("fullName");
                     storage.removeItem("user_status");
                     storage.removeItem("store_uuidExist");
                     createDataBase();
                     launchCheckUUIDExistFlag = 0;
                 }
             }
         }
     });
 }