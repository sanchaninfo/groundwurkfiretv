/*duration of movie or tv shows*/
function durationOfAsset(duration) {
     var timeSplitArray = []
    timeSplitArray = timeConversion(duration).split(",");
    if (timeSplitArray[0] == 0) {
        $('#timeSpan').html(timeSplitArray[1] + "m");

    } else {
        $('#timeSpan').html(timeSplitArray[0] + "h " + timeSplitArray[1] + "m");

    }
}

/*function for getting year of release*/
function yearOfRelease(yearValue) {

    var tempArray = [];
    tempArray = yearValue.split("-");
    return tempArray[0];

}

function timeConversion(timeDuration){
               var ms=parseInt(timeDuration); 
               var seconds = (ms/1000);
               var minutes = parseInt(seconds/60, 10);
               seconds = seconds%60;
               var hours = parseInt(minutes/60, 10);
               minutes = minutes%60;
               var timeReturn = hours+','+minutes;
               return timeReturn;
}