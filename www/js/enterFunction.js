/*
     Module Name :
     File Name   :      enterFunction.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0 
     Created on  :      8 august 12:26 pm
     Last modified on:  1st November
     Description :      functionality for enter click
     Organisation:      Peafowl inc.
*/
function enterClickFunction() {
    var storage = window.localStorage;
    isDescriptionDivVisible = $('#descriptionDivDescribe').is(':visible');
    isThumbsListVisible = $('#thumbsList').is(':visible');
    var isEpisodeThumbsVisible = $('#episodeButtons').is(':visible');
    var isEpisode3Visible = $('#episode_action3').is(':visible');
    var isEpisodeResumeVisible = $('#episode_actionResume').is(':visible');
    var ismenuDiv = $('#menuDiv').is(':visible');
    var catVisible = $('#Catecomplete_' + countDownClickVar).is(':visible');
    var catSelectVisible = $('#categorySelectMain').is(':visible');
    var videoDivVisible = $('#video_player').is(':visible');
    var searchDivVisible = $('#divSearch').is(':visible');
    var searchBorder = $('#divimageBorderSearch').is(':visible');
    var settingsDiv = $('#divSettingsMain').is(':visible');
    var videoRedirectIsVisible = $('#videoRedirect').is(':visible');
      var Store = window.localStorage;
   // var $visibleForm = $('form:visible'),
   // formId = $visibleForm.attr('id');
    //alert("visible div "+$( "div:visible" ).attr('id'));
    if (isEpisodeThumbsVisible) {

        var episodeValue = $("#Episode_" + episodeDiv + "_" + episodeAssetDownClick).attr("title");
        var epiAssetVal = [];
        epiAssetVal = episodeValue.split('|');
        videoPlayerInEpisodes = 1;
        playVidInEpisodes = 1;
        episodeVideoPlay(epiAssetVal[0], epiAssetVal[1]);
    }else if(storeSliderVisible == 1){
        enterClick();
      
    }
     else if (catVisible == true && ismenuDiv == false) {
         var storage = window.localStorage;

        storage.setItem(resumeFlag, "false");
        if (descriptionFlag == 0 || descriptionFlag == 1) {
            flagMainDivVisible = 1;
            flagAddListBack = 0;
            var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
            $("#catVisible").fadeOut();
            $("#descTxtCat").fadeOut();
            $("#Catecomplete_" + countDownClickVar).fadeOut();
            $("#image1").fadeOut();
            $(".preloaderContDescribe").fadeIn();
            var splitAssetDetails = [];
            splitAssetDetails = assetDataValues.split('|');
            if(Store.getItem(userId) == null ){
                        createDataBase();
            }
            else{
                 displayOfInnerDescribedPage(storingIn, splitAssetDetails[0]);
           }
           
        }
    } else if (ismenuDiv) {
       // $("#menuDiv").fadeOut();
       categoryVisibleChange = true;
   $("#menuDiv").animate({
            left:"-25%"   
        }, 500,  function() {
             $("#image1").fadeIn("slow");
             $("#descTxtCat").fadeIn();
        }).hide();
    $("#CategroyDataDiv").animate({
            right:"0px"   
        }, 500);

    $("#Catecomplete_" + countDownClickVar).animate({
            marginLeft:"116px"
        }, 500);
        assetDetailsSplitCat(assetDataValues);
    } else if (isThumbsListVisible) {
          var storage = window.localStorage;
          var resumeFlagVal = storage.getItem(resumeFlag);
           // alert("resume "+resumeFlagVal);
        storage.setItem(resumeFlag, "false");
        if (descriptionFlag == 0 || descriptionFlag == 1) {
           /*  if(countDownClickVarLanding == cnt-1){
               
             $("#descTxt").fadeOut();
                $("#desc_cont_landingPage").fadeOut();
               // $("#movieThumbnails").fadeOut();
                $('#movieThumbnails').css({ opacity: 1 });
                $("#divID").fadeOut();
                $("#landing_BackgroundImage").fadeOut();
                $("#landingPage").hide();
               // $(".gradient_class").hide();
                //$(".landingPage_blackscreen").hide();
                $("#MainDiv").css({ opacity: 0 });
                $('#slideShowImg').css({ opacity: 1 });
                slideShower = 1;
                mainDivFlag = 0;
           // $("#slideShowImg").show();
            // $("#reg_Slide4").show();
             /* $("#descTxt").fadeOut();
                $("#desc_cont_landingPage").fadeOut();
                $("#movieThumbnails").fadeOut();
                $("#divID").fadeOut();
                $("#landing_BackgroundImage").fadeOut();
                $(".gradient_class").hide();
                $(".landingPage_blackscreen").hide();
                startStore();
                $("#store").show();*/
        //}
        /*if(countDownClickVarLanding == cnt-1){
            window.location.href = 'indexDevStore.html';
        }*/
       // else{
           
                var subscriptionValue = window.localStorage;
                flagMainDivVisible = 0;
                flagAddListBack = 0;
                //var networkCheck = checkNetworkConnectivity();
                var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails = [];
                splitAssetDetails = assetValueTest.split('|');
                console.log("splitAssetDetails[0] "+splitAssetDetails[0])
            //if(networkCheck == 1){
               if(Store.getItem(userId) == null ){
                        createDataBase();
            }
            else{
                 displayOfInnerDescribedPage(storingIn, splitAssetDetails[0]);
           }
                // displayOfInnerDescribedPage(storingIn, splitAssetDetails[0]);
                $("#descTxt").fadeOut();
                $("#desc_cont_landingPage").fadeOut();
                $("#movieThumbnails").fadeOut();
                $("#divID").fadeOut();
                $(".preloaderContDescribe").fadeIn();
           // }
       // }
        }
    } else if (catSelectVisible) {
        if (catSelectCount == 1) { //selection of search page
            $("#landingPage").fadeOut();
            $("#categorySelectMain").fadeOut();
            $("#divSearch").fadeIn();
            $("#1g").css({
                color: 'black'
            });;
        } else if (catSelectCount == 2) { //selection of category page
            var storage = window.localStorage;
            storage.setItem(resumeFlag, "true");
            $("#landingPage").fadeOut();
            $("#categorySelectMain").fadeOut();
            $("#catBackground").fadeIn();
            $("#CategoryPage").fadeIn();
            $("#menuDiv").fadeIn();
            $("#Catedata_" + countDownClickVar).fadeIn();
            $("#Catecomplete_" + countDownClickVar).fadeIn();
            $("#Catedata_" + countDownClickVar).addClass("cate_buttonactive");
            var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
            assetDetailsSplitCat(assetData);
        } else if (catSelectCount == 3) { //selection of settings page
            var storage = window.localStorage;
             if(Store.getItem(userId) == null ){
                        createDataBase();
            }
            else{

                  var dateFormat = new Date(storage.getItem(subscription_end));
            var date = dateFormat.getDate();
            var year = dateFormat.getFullYear();
            var month = dateFormat.getMonth()+1;
            $("#landingPage").fadeOut();
            $("#categorySelectMain").fadeOut();
            $("#divSettingsMain").fadeIn();
            $(".videoContEmail").html(storage.getItem(email));
            var strPlan = storage.getItem(subscription_type);
            strPlan = strPlan.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            $(".videoContPlan").html(strPlan);
            if(storage.getItem(subscription_end) == ""){
                $(".videoContExpiry").html("");
            }
            else{
                $(".videoContExpiry").html(month + "/" + date + "/" + year);
            }
            
            }
          
        } else if (catSelectCount == 4) { //selection for exiting damedash studios
            navigator.app.exitApp();
        }
    }
    if (episodeClick == 3) {
       
        if (isEpisode3Visible) {
           
            if($("#episode_action"+episodeClick).hasClass("star_active1")){
               var activeClassCountAvg =0;
               var storage = window.localStorage;
                flagAddList = 1;
                  $.ajax({
                    type: "POST",
                   // url: URL_LINK + "/userAssetRating",


                    url: ServerLamdaUrl + "userassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName+ "&user_rating=" + startRateCount+ "&previousrate=" + userratingsum,
                    /*data: JSON.stringify({
                        "userAssetRating": {
                            "assetId": globalassetId,
                            "userId": storingIn,
                            "rating":startRateCount,
                            "previousRate":userratingsum
                        }
                    }),*/
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {

                        $.ajax({
                    type: "POST",
                    //url: URL_LINK + "/getAssetRating",
                    url: ServerLamdaUrl + "getassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (dataRate) {
                        var data = dataRate.result;
                       // addOrRemoveAssetToList(data.assetId, data, data.userData[0].myList, flagNextAsset);
                       userratingsum = data[0].userassetrating;
                       startRateCount = data[0].userassetrating;
                       avgAssetRating = data[0].averagerating;
                       
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                       
                          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                         
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                    userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);

                    }
                });
                    }
                });
            }
            else{
               
                   if (resumeEpisodeStat == 1) {
                if (divVisibleLanding == 1) {
                    var assetValueTest1 = $('#assetid' + countDownClickVarLanding).val();
                    var splitAssetDetails1 = [];
                    splitAssetDetails1 = assetValueTest1.split('|');
                    flagButtonMovement = 1;
                    addOrRemoveFromMyList(splitAssetDetails1[0],0);
                } else if (divVisibleLanding == 0) {
                    if (flagSearchDescribe == 1) {
                        var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                        var splitAssetDetailsSearch = [];
                        splitAssetDetailsSearch = assetValueSearch.split('|');
                        flagButtonMovement = 1;
                        addOrRemoveFromMyList(splitAssetDetailsSearch[0],0);
                    } else {
                        var assetValueTest1 = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                        var splitAssetDetails1 = [];
                        splitAssetDetails1 = assetValueTest1.split('|');
                        flagButtonMovement = 1;
                        addOrRemoveFromMyList(splitAssetDetails1[0],0);
                    }

                }
            } else {
                if($("#episode_action"+episodeClick).hasClass("episodes_landingPage")){
                   
                     landingCarouselFlag = 0;
                episodeCarouselFlag = 1;
                $("#descriptionDiv").fadeOut();
                if (pageVisible == 0) {

                    var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                    var splitAssetDetails = [];
                    splitAssetDetails = assetValueTest.split('|');
                    //$('#department').load(getEpisodedetails(splitAssetDetails[0]));
                    $("#landingPage").fadeOut();
                    getEpisodedetails(splitAssetDetails[0]);
                } else {
                    if (flagSearchDescribe == 1) {
                        var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                        var splitAssetDetailsSearch = [];
                        splitAssetDetailsSearch = assetValueSearch.split('|');
                        $("#landingPageSearch").fadeOut();
                        getEpisodedetails(splitAssetDetailsSearch[0]);
                    } else {
                        var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                        var splitAssetDetails = [];
                        splitAssetDetails = assetDataValues.split('|');
                        $("#catBackground").fadeOut();
                        getEpisodedetails(splitAssetDetails[0]);
                    }
                }
                }
                else{
                   
                     var assetValueTest1 = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails1 = [];
                splitAssetDetails1 = assetValueTest1.split('|');
                alert(splitAssetDetails1[0])
                //addOrRemoveFromMyList(splitAssetDetails1[0],0);

                   if(Store.getItem(userId) == null ){
                        createDataBase();
                       }
                       else{
                         addOrRemoveFromMyList(splitAssetDetails1[0],0);
                       }
                }
                
            }
            }
         
        } else if (isEpisodeResumeVisible == true) {

            if (divVisibleLanding == 1) {
                var assetValueTest1 = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails1 = [];
                splitAssetDetails1 = assetValueTest1.split('|');
                flagButtonMovement = 1;
              //  addOrRemoveFromMyList(splitAssetDetails1[0],0);

                   if(Store.getItem(userId) == null ){
                        createDataBase();
                       }
                       else{
                         addOrRemoveFromMyList(splitAssetDetails1[0],0);
                       }
            } else if (divVisibleLanding == 0) {
                if (flagSearchDescribe == 1) {
                    var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                    var splitAssetDetailsSearch = [];
                    splitAssetDetailsSearch = assetValueSearch.split('|');
                    flagButtonMovement = 1;
                   // addOrRemoveFromMyList(splitAssetDetailsSearch[0],0);

                    if(Store.getItem(userId) == null ){
                        createDataBase();
                       }
                       else{
                         addOrRemoveFromMyList(splitAssetDetailsSearch[0],0);
                       }
                } else {
                    var assetValueTest1 = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                    var splitAssetDetails1 = [];
                    splitAssetDetails1 = assetValueTest1.split('|');
                    flagButtonMovement = 1;
                   // addOrRemoveFromMyList(splitAssetDetails1[0],0);

                     if(Store.getItem(userId) == null ){
                        createDataBase();
                       }
                       else{
                        addOrRemoveFromMyList(splitAssetDetails1[0],0);
                       }
                }

            }
        }

    } else if (episodeClick == 4) {
        
        if($("#episode_action"+episodeClick).hasClass("star_active1")){
                var storage = window.localStorage;
                var activeClassCountAvg =0;
                flagAddList = 1;
                  $.ajax({
                    type: "POST",
                     url: ServerLamdaUrl + "userassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName+ "&user_rating=" + startRateCount+ "&previousrate=" + userratingsum,
                    /*data: JSON.stringify({
                        "userAssetRating": {
                            "assetId": globalassetId,
                            "userId": storingIn,
                            "rating":startRateCount,
                            "previousRate":userratingsum
                        }
                    }),*/
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                       
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                          $.ajax({
                    type: "POST",
                    //url: URL_LINK + "/getAssetRating",
                    url: ServerLamdaUrl + "getassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (dataRate) {
                        var data = dataRate.result;
                       // addOrRemoveAssetToList(data.assetId, data, data.userData[0].myList, flagNextAsset);
                       userratingsum = data[0].userassetrating;
                       startRateCount = data[0].userassetrating;
                       avgAssetRating = data[0].averagerating;
                       
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                       
                          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                         
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                    userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);

                    }
                });

                    }
                });
            }
            else{
                 if (isEpisode3Visible) {
                    if($("#episode_action"+(episodeClick-1)).hasClass("star_active1")){
                        var storage = window.localStorage;
                        var activeClassCountAvg =0;
                        flagAddList = 1;
                         $.ajax({
                    type: "POST",
                    url: ServerLamdaUrl + "userassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName+ "&user_rating=" + startRateCount+ "&previousrate=" + userratingsum,
                    /*data: JSON.stringify({
                        "userAssetRating": {
                            "assetId": globalassetId,
                            "userId": storingIn,
                            "rating":startRateCount,
                            "previousRate":userratingsum
                        }
                    }),*/
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                           $.ajax({
                    type: "POST",
                    //url: URL_LINK + "/getAssetRating",
                    url: ServerLamdaUrl + "getassetrating?assetId="+globalassetId+"&token=" + storage.getItem(accessToken) + "&appname=" + appName,
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (dataRate) {
                        var data = dataRate.result;
                       // addOrRemoveAssetToList(data.assetId, data, data.userData[0].myList, flagNextAsset);
                       userratingsum = data[0].userassetrating;
                       startRateCount = data[0].userassetrating;
                       avgAssetRating = data[0].averagerating;
                       
                      
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    complete: function () {
                       
                          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                         
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                    userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);

                    }
                });
                    }
                });
                    }
                    else{
                         
                        landingCarouselFlag = 0;
            episodeCarouselFlag = 1;
            $("#descriptionDiv").fadeOut();
            $("#episodeDescription").fadeIn();
            if (pageVisible == 0) {
                var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails = [];
                $("#landingPage").fadeOut();
                splitAssetDetails = assetValueTest.split('|');
                getEpisodedetails(splitAssetDetails[0]);
            } else {
                if (flagSearchDescribe == 1) {
                    var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                    var splitAssetDetailsSearch = [];
                    splitAssetDetailsSearch = assetValueSearch.split('|');
                    $("#landingPageSearch").fadeOut();
                    getEpisodedetails(splitAssetDetailsSearch[0]);
                } else {
                    var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                    var splitAssetDetails = [];
                    splitAssetDetails = assetDataValues.split('|');
                    $("#catBackground").fadeOut();
                    getEpisodedetails(splitAssetDetails[0]);
                }
            }
        }
                    }
            
            }
       
    } else if (isDescriptionDivVisible) {

        if (episodeClick == 1) {

            if (divVisibleLanding == 1) {
                if (isEpisodeResumeVisible || resumeEpisodeStat == 1) {
                    
                    var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                    var splitAssetDetails = [];
                    splitAssetDetails = assetValueTest.split('|');
                   // resumeButtonVisible(splitAssetDetails[0]);
                    if(monetize == "true"){
                        
                        videoPlayWithStore(splitAssetDetails[0],1);
                    }else{
                          if(Store.getItem(userId) == null ){
                        createDataBase();
                    }
                    else{
                        
                        resumeButtonVisible(splitAssetDetails[0]);
                    }
                        //resumeButtonVisible(splitAssetDetails[0]);
                    }
                } else {
                    var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                    var splitAssetDetails = [];
                    splitAssetDetails = assetValueTest.split('|');
                    if(monetize == "true" ){
                        
                        videoPlayWithStore(splitAssetDetails[0],0);
                    }else{
                       // updateSeekTimeAndPlay(splitAssetDetails[0]);
                        if(Store.getItem(userId) == null ){
                        createDataBase();
                    }
                    else{
                        
                        updateSeekTimeAndPlay(splitAssetDetails[0]);
                    }
                    }                    
                }
            } else if (divVisibleLanding == 0) {
                if (isEpisodeResumeVisible) {
                    if (flagSearchDescribe == 1) {
                        var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                        var splitAssetDetailsSearch = [];
                        splitAssetDetailsSearch = assetValueSearch.split('|');
                        //resumeButtonVisible(splitAssetDetailsSearch[0]);
                        if(monetize == "true"){
                             
                        videoPlayWithStore(splitAssetDetailsSearch[0],1);
                    }else{
                       // resumeButtonVisible(splitAssetDetailsSearch[0]);
                          if(Store.getItem(userId) == null ){
                        createDataBase();
                    }
                    else{
                        
                        resumeButtonVisible(splitAssetDetailsSearch[0]);
                    }
                    }
                    } else {
                        var assetValueTest = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                        var splitAssetDetails = [];
                        splitAssetDetails = assetValueTest.split('|');
                        //resumeButtonVisible(splitAssetDetails[0])
                         if(monetize == "true"){
                           
                        videoPlayWithStore(splitAssetDetails[0],1);
                    }else{
                       // resumeButtonVisible(splitAssetDetails[0]);
                           if(Store.getItem(userId) == null ){
                        createDataBase();
                    }
                    else{
                        
                       resumeButtonVisible(splitAssetDetails[0]);
                    }
                    }
                    }
                } else {
                   
                    if (flagSearchDescribe == 1) {
                        var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                        var splitAssetDetailsSearch = [];
                        splitAssetDetailsSearch = assetValueSearch.split('|');
                         if(monetize == "true"){
                          
                        videoPlayWithStore(splitAssetDetailsSearch[0],0);
                    }else{
                       // updateSeekTimeAndPlay(splitAssetDetailsSearch[0]);
                            if(Store.getItem(userId) == null ){
                        createDataBase();
                    }
                    else{
                        
                       updateSeekTimeAndPlay(splitAssetDetailsSearch[0]);
                    }
                    } 
                    } else {
                        var assetValueTest = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                        var splitAssetDetails = [];
                        splitAssetDetails = assetValueTest.split('|');
                        if(monetize == "true"){
                            
                        videoPlayWithStore(splitAssetDetails[0],0);
                        }else{
                        //updateSeekTimeAndPlay(splitAssetDetails[0]);
                              if(Store.getItem(userId) == null ){
                        createDataBase();
                    }
                    else{
                        
                       updateSeekTimeAndPlay(splitAssetDetails[0]);
                    }
                        }
                    }
                }
            }
        } else if ((episodeClick == 2 || isEpisode3Visible) && isEpisodeResumeVisible == false) {
            
            if (divVisibleLanding == 1) {

                var assetValueTest1 = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails1 = [];
                splitAssetDetails1 = assetValueTest1.split('|');
                flagButtonMovement = 1;
               //addOrRemoveFromMyList(splitAssetDetails1[0],0);
               addOrRemoveFromMyList(splitAssetDetails1[0],0);
               /* if(Store.getItem(userId) == null ){
                        createDataBase();
                       }
                       else{
                        alert('test')
                         alert('test split '+splitAssetDetails1[0]);
                         addOrRemoveFromMyList(splitAssetDetails1[0],0);
                       }*/
               

            } else if (divVisibleLanding == 0) {
                if (flagSearchDescribe == 1) {
                    var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                    var splitAssetDetailsSearch = [];
                    splitAssetDetailsSearch = assetValueSearch.split('|');
                    flagButtonMovement = 1;
                   // addOrRemoveFromMyList(splitAssetDetailsSearch[0],0);
                    if(Store.getItem(userId) == null ){
                        createDataBase();
                       }
                       else{
                         addOrRemoveFromMyList(splitAssetDetailsSearch[0],0);
                       }
               
                } else {
                    var assetValueTest1 = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                    var splitAssetDetails1 = [];
                    splitAssetDetails1 = assetValueTest1.split('|');
                    flagButtonMovement = 1;
                  //  addOrRemoveFromMyList(splitAssetDetails1[0],0);
                   if(Store.getItem(userId) == null ){
                        createDataBase();
                       }
                       else{
                         addOrRemoveFromMyList(splitAssetDetails1[0],0);
                       }
                }

            }

        } else if (episodeClick == 2 && isEpisodeResumeVisible == true) {
            
            if (divVisibleLanding == 1) {
                var assetValueTest = $('#assetid' + countDownClickVarLanding).val();
                var splitAssetDetails = [];
                splitAssetDetails = assetValueTest.split('|');
               // updateSeekTimeAndPlay(splitAssetDetails[0]);
                if(monetize == "true" ){
                        videoPlayWithStore(splitAssetDetails[0],0);
                    }else{
                       // updateSeekTimeAndPlay(splitAssetDetails[0]);
                                 if(Store.getItem(userId) == null ){
                        createDataBase();
                    }
                    else{
                        
                       updateSeekTimeAndPlay(splitAssetDetails[0]);
                    }
                    }
            } else if (divVisibleLanding == 0) {
                if (flagSearchDescribe == 1) {
                    var assetValueSearch = $("#SearchList_" + searchCountAsset).attr("title");
                    var splitAssetDetailsSearch = [];
                    splitAssetDetailsSearch = assetValueSearch.split('|');
                     if(monetize == "true"){
                        videoPlayWithStore(splitAssetDetailsSearch[0],0);
                    }else{
                       // updateSeekTimeAndPlay(splitAssetDetailsSearch[0]);
                                   if(Store.getItem(userId) == null ){
                        createDataBase();
                    }
                    else{
                        
                       updateSeekTimeAndPlay(splitAssetDetailsSearch[0]);
                    }
                    }
                } else {
                    var assetValueTest = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                    var splitAssetDetails = [];
                    splitAssetDetails = assetValueTest.split('|');
                    if(monetize == "true" ){
                        videoPlayWithStore(splitAssetDetails[0],0);
                    }else{
                       // updateSeekTimeAndPlay(splitAssetDetails[0]);
                                    if(Store.getItem(userId) == null ){
                        createDataBase();
                    }
                    else{
                        
                       updateSeekTimeAndPlay(splitAssetDetails[0]);
                    }
                    }
                }
            }
        }

    } else if (videoDivVisible) {
        if(storeSliderVisible != 1){
            videoFileId.setAttribute("controls", "controls")
        setTimeout(function() {
            videoFileId.removeAttribute("controls");
        }, 1000);
        if (videoFileId.paused) {
            videoFileId.play();
        } else {
            flagValuePause = 1;
            videoFileId.pause();
        }
        }
        
    } else if (searchDivVisible == true) {
        if (searchBorder) {
            flagSearchDescribe = 1;
            var assetValueTest = $('#SearchList_' + searchCountAsset).attr("title");
            console.log("assetValueTest "+assetValueTest);
            var splitAssetDetails = [];
            splitAssetDetails = assetValueTest.split('|');
            $("#divSearch").fadeOut();
            $(".preloaderContDescribe").fadeIn();
            $("#landingPageSearch").fadeIn();
            $('#landing_BackgroundImageSearch').css({
                'background-image': "url(" + splitAssetDetails[6] + ")"
            });
             if(Store.getItem(userId) == null ){
                        createDataBase();
            }
            else{
                 displayOfInnerDescribedPage(storingIn, splitAssetDetails[0]);
           }
           // displayOfInnerDescribedPage(storingIn, splitAssetDetails[0]);
        } else {
            var activeId = $('.searchCont_active').attr('id');
            if (activeId == "divKeyBoardBackBtn") {
                $("#divSearch").fadeOut();
                $("#categorySelectMain").fadeIn();
                $("#landingPage").fadeIn();
            } else {
                var searchData = "";
                $("#ulAssetContsearchDivData").html("");
                searchCountValue = 0;
                var $write = $('#h2AssetContsearchInput');

                var assetVal = $("#" + keyPadCount + "g").attr("title");
                if (assetVal == "delete") {
                    var html = $write.html();
                    $write.html(html.substr(0, html.length - 1));
                } else if (assetVal == "space") {
                    character = ' ';
                    if($write.html() == "") {
                          //call function

                    }
                    else{
                         $write.html($write.html() + character);
                    }
                   
                } else {
                    character = assetVal;
                    $write.html($write.html() + character);
                }
                var filter = $("#h2AssetContsearchInput").html();

                var carouselArrayNew = jQuery.grep(carouselArray, function(n, i) {
                    if (n.assetName.search(new RegExp(filter, "i")) < 0) {
                        $("#searchAssets").show();
                        return 0;
                    } else if (filter == "") {
                        
                    } else {
                       $("#searchAssets").hide();
                        searchCountValue++;
                        var searchCountValueId = searchCountValue;
                        if (searchCountValueId == searchCountValue) {
                            
                        if(n.assetImage == ""){
                                var searchLstIdVal = 'SearchList_' + searchCountValue;
                            var carodatacontent = n.assetId + '|' + n.assetName + '|' + n.assetDescription + '|' + n.assetFileDuration + '|' + n.assetDirector + '|' + n.assetCast + '|' + n.assetImageMain;
                            searchData = searchData + '<li  class="vid" title="' + carodatacontent + '" id="SearchList_' + searchCountValueId + '"  ><img  src="./images/thumb.jpg"  /></li>';
                            }
                            else{
                                 var searchLstIdVal = 'SearchList_' + searchCountValue;
                            var carodatacontent = n.assetId + '|' + n.assetName + '|' + n.assetDescription + '|' + n.assetFileDuration + '|' + n.assetDirector + '|' + n.assetCast + '|' + n.assetImageMain;
                            searchData = searchData + '<li  class="vid" title="' + carodatacontent + '" id="SearchList_' + searchCountValueId + '"  ><img  src="' + n.assetImage + '"  /></li>';
                            }

                          /*  var searchLstIdVal = 'SearchList_' + searchCountValue;
                            var carodatacontent = n.assetId + '|' + n.assetName + '|' + n.assetDescription + '|' + n.assetFileDuration + '|' + n.assetDirector + '|' + n.assetCast + '|' + n.assetImageMain;
                            searchData = searchData + '<li  class="vid" title="' + carodatacontent + '" id="SearchList_' + searchCountValueId + '"  ><img  src="' + n.assetImage + '"  /></li>';
                           */
                           // $("#searchAssets").hide();
                            $("#ulAssetContsearchDivData").html(searchData);




                        }
                        else{
                          $("#searchAssets").hide();
                        }

                    }

                });

            }

        }

    } else if (settingsDiv) {
        if (settingsClick == 0) {
            $("#divSettingsMain").fadeOut();
            $("#categorySelectMain").fadeIn();
        } else if (settingsClick == 1) {
            navigator.app.exitApp();
        }
    }
    else if(videoRedirectIsVisible){
       
       if($( "#videFunc0" ).hasClass( "videFuncActive" )){
         $("#video_player").html('');
         $("#videoRedirect").hide();
          $("#buttonShow").show();
        $(".ddlogobottom").show();
         assetNextFlag = 0;
        //$(".thumbsCont_landingPage").hide();
        $("#title").show();
        $("#movieDescribe").show();
        $("#divID").show();
         $("#descriptionDivDescribe").show();
        $("#divIDImg").hide();
      //  $("#landingPageSearch").fadeIn();

        if (flagMainDivVisible == 0) {
            if (flagSearchDescribe == 1) {
                $("#landingPageSearch").fadeIn();
            } else {
                $("#landingPage").fadeIn();
            }
        } else {
            $("#catBackground").fadeIn();
        }
        //$("#landingPage").fadeOut();
       //$("#landingPageSearch").fadeOut();
        clearInterval(myVarNext);
       
       /*  clearInterval(myVarNext);
        assetNextFlag = 0;
                    $("#video_player").hide();
                    $("#videoLoad").hide();
                    $("#descriptionDivDescribe").show();
                    if (flagMainDivVisible == 0) {
                        if (flagSearchDescribe == 1) {
                            $("#landingPageSearch").show();
                        } else {
                            $("#landingPage").show();
                        }
                    } else {
                        $("#catBackground").show();
                    }

                    $("#buttonShow").show();
                    $(".ddlogobottom").show();
                    //$(".blackscreen_landingPage").show();
                    $("#movieDescribe").show();
                    $("#title").show();
                    $("#divID").show();
                    clearInterval(myVar);
                    if (flagResumePlay == 1) {
                        flagResumePlay = 0;
                        clearInterval(myVarTimeUpdate);
                    }*/
       }
       else if($( "#videFunc1" ).hasClass( "videFuncActive" )){
         if($( "#child0" ).hasClass( "VEplaybtn_active" )){
            $('#videoRedirect').hide();
            assetNextFlag = 1;
            playNextFlag = 1;
            varCountNext = 15;
            flagNextPlay =0;
            //updateSeekTimeAndPlay(assetNextId)
           // clearInterval(myVar);
            //displayOfInnerDescribedPage(storingIn,assetNextId);
              
         }
         else if($( "#child1" ).hasClass( "VEplaybtn_active" )){
            addOrRemoveFromMyList(assetNextId,1);
         } 
       }  
    }
}