/*
     Module Name :
     File Name   :      descriptionDisplayFormat.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0 // written by sachin for present.
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      function for buttons display format in description page depending on the asset data.
     Organisation:      Peafowl inc.
*/
function addingToMyListDisplayFun(imageValue, nameValue, description, durationVal, listYear, buttonFormat) {

    $('#descriptionDiv').html('<div id="descriptionDivDescribe" class="desc_txt_landingPage"><ul><li id="title" class="mainTitle_landingPage"></li><li><span id="ratingDisplay" ></span> <span class="movieDesc_landingPage" id="movieReleaseDisp"></span><span id="timeSpan" class="movieDesc_landingPage"></span><span  id="moviegenre" class="movieDesc_landingPage"></span></li><li><p id="movieDescribe" class="landing_synopys"></p></li></ul><div id="buttonShow" class="showbuttons_1" ><ul><li id="episode_action1"  class="ps1_landingPage" > Play </li><li id="episode_action2"  class="audiosub">  Audio and Subtitles </li><li id="episode_action3"  class="addlist">Add to my list</li><li id="episode_action4" class="rating" >Rate this Title </li></ul></div></div>');
    $("#descriptionDiv").fadeIn();
    $('#buttonShow').fadeIn();
    $('#divIDImg').fadeIn();
    $("#title").html(nameValue);
   // $('#rating').html('<div id = "rate1" class="ratingStarDisp"></div><div id = "rate2" class="ratingStarDisp"></div><div id = "rate3" class="ratingStarDisp"></div><div id = "rate4" class="ratingStarDisp"></div><div id = "rate5" class="ratingStarDisp"></div>');
    $("#movieDescribe").html(description.replace(/[^a-zA-Z 0-9]+/g, ""));
 var activeClassCountAvg =0;
   // durationOfAsset(durationVal);
    $('#movieReleaseDisp').html(yearOfRelease(listYear));
    if (buttonFormat == "1") {
        $('#buttonShow').html('<ul><li  id="episode_actionResume"  class="ps1_landingPageResume" > Resume </li><ul><li  id="episode_action1"  class="ps1_landingPage" > Play </li><li id="episode_action2" class="addlist1">Remove From My List</li><li id="episode_action3"  class="rating">Rate this title</li>');
        $("#episode_actionResume").addClass("ps1_landingPageResume_active");
        $("#episode_actionResume").css({
            color: 'black'
        });
        flagDivVal = 1;
        flagEnterVariable = 1;
        flagRemVal = 0;
        flagAssetStatus = 1;
        resumeEpisodeStat = 0;
        // $('#rating').html('<div id = "rate1" class="ratingStarDisp"></div><div id = "rate2" class="ratingStarDisp"></div><div id = "rate3" class="ratingStarDisp"></div><div id = "rate4" class="ratingStarDisp"></div><div id = "rate5" class="ratingStarDisp"></div>');
          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                         
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                    userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);
    } else if (buttonFormat == "2") {
        $('#buttonShow').html('<ul><li  id="episode_action1"  class="ps1_landingPage" > Play </li><li id="episode_action2" class="addlist1">Remove From My List</li><li id="episode_action3"  class="rating">Rate this title</li>');
        $("#episode_action1").addClass("ps1_landingPage_active");
        $("#episode_action1").css({
            color: 'black'
        });
          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                      userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);
        flagDivVal = 1;
        flagEnterVariable = 1;
        flagRemVal = 0;
        flagAssetStatus = 1;
        resumeEpisodeStat = 0;
    } else if (buttonFormat == "3") {
        $('#buttonShow').html('<ul><li  id="episode_actionResume"  class="ps1_landingPageResume" > Resume </li><ul><li  id="episode_action1"  class="ps1_landingPage" > Play </li><li id="episode_action2" class="addlist">Add To My List</li><li id="episode_action3"  class="rating">Rate this title</li>');
        $("#episode_actionResume").addClass("ps1_landingPageResume_active");
        $("#episode_actionResume").css({
            color: 'black'
        });
        //flagDivVal=1;
          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                     userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);
        flagEnterVariable = 0;
        flagRemVal = 1;
        flagEpisodeVal = 1;
        flagAssetStatus = 0;
        resumeEpisodeStat = 0;
    } else if (buttonFormat == "4") {

        $('#buttonShow').html('<ul><li  id="episode_action1"  class="ps1_landingPage" > Play </li><li id="episode_action2" class="addlist">Add To My List</li><li id="episode_action3"  class="rating">Rate this title</li>');
        $("#episode_action1").addClass("ps1_landingPage_active");
        $("#episode_action1").css({
            color: 'black'
        });
          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                     userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);
        flagEnterVariable = 0;
        flagRemVal = 1;
        flagAssetStatus = 0;
        resumeEpisodeStat = 0;
    } else if (buttonFormat == "5") {
        $('#buttonShow').html('<ul><li   id="episode_action1"  class="ps1_landingPage" > Play Season ' + seasonNumValue + ': Episode ' + episodeNumValue + '</li><li id="episode_action2" class="addlist1">Remove From My List</li><li id="episode_action3" class="episodes_landingPage">Episodes and More</li><li id="episode_action4"  class="rating">Rate this title</li>');
        $("#episode_action1").addClass("ps1_landingPage_active");
        $("#episode_action1").css({
            color: 'black'
        });
          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                      userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);
        flagDivVal = 1;
        flagEnterVariable = 1;
        flagRemVal = 0;
        flagAssetStatus = 1;
        resumeEpisodeStat = 0;
    } else if (buttonFormat == "6") {
        $('#buttonShow').html('<ul><li   id="episode_action1"  class="ps1_landingPage" > Play Season ' + seasonNumValue + ': Episode ' + episodeNumValue + '</li><li id="episode_action2" class="addlist">Add to My List</li><li id="episode_action3" class="episodes_landingPage">Episodes and More</li><li id="episode_action4"  class="rating">Rate this title</li>');
        $("#episode_action1").addClass("ps1_landingPage_active");
        $("#episode_action1").css({
            color: 'black'
        });
          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                      userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);
        flagEnterVariable = 0;
        flagRemVal = 1;
        flagAssetStatus = 0;
        resumeEpisodeStat = 0;
    } else if (buttonFormat == "7") {
        $('#buttonShow').html('<ul><li  id="episode_actionResume"  class="ps1_landingPageResume" > Resume </li><li   id="episode_action1"  class="ps1_landingPage" > Play Season ' + seasonNumValue + ': Episode ' + episodeNumValue + '</li><li id="episode_action2" class="addlist1">Remove From My List</li><li id="episode_action3" class="episodes_landingPage">Episodes and More</li><li id="episode_action4"  class="rating">Rate this title</li>');
        $("#episode_actionResume").addClass("ps1_landingPageResume_active");
        $("#episode_actionResume").css({
            color: 'black'
        });
          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                     userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);
        flagDivVal = 1;
        flagEnterVariable = 1;
        flagRemVal = 0;
        flagAssetStatus = 1;
        resumeEpisodeStat = 1;
    } else if (buttonFormat == "8") {
        $('#buttonShow').html('<ul><li  id="episode_actionResume"  class="ps1_landingPageResume" > Resume </li><li   id="episode_action1"  class="ps1_landingPage" > Play Season ' + seasonNumValue + ': Episode ' + episodeNumValue + '</li><li id="episode_action2" class="addlist">Add to My List</li><li id="episode_action3" class="episodes_landingPage">Episodes and More</li><li id="episode_action4"  class="rating">Rate this title</li>');
        $("#episode_actionResume").addClass("ps1_landingPageResume_active");
        $("#episode_actionResume").css({
            color: 'black'
        });
          var userRatingHtml = "";
                    for(var assetRating = 0;assetRating<6;assetRating++){
                        if(avgAssetRating>activeClassCountAvg){
                            activeClassCountAvg++;
                             userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star_active"></div>';
                        }
                        else{                        
                            userRatingHtml = userRatingHtml + '<div id = "rate'+(assetRating+1)+'" class="star"></div>';                          
                        }
                    }
                      userRatingHtml = userRatingHtml + '<div id="avgRat">'+(avgAssetRating)+'/6</div>'
                   $('#ratingDisplay').html(userRatingHtml);
        flagEnterVariable = 0;
        flagRemVal = 1;
        flagAssetStatus = 0;
        resumeEpisodeStat = 1;
    }
    $(".preloaderContDescribe").fadeOut();
}