/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
          document.addEventListener("offline", this.onOffline, false);
         document.addEventListener("online", this.onOnline, false);
         document.addEventListener("resume", this.onResume, false);

        // document.addEventListener("deviceready",this.onDatabaseCreation , false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        //document.addEventListener("resume", this.onResume, false);
    },

   onResume: function() {
        
        window.location.href = 'index.html';
    },
   
    onDatabaseCreation: function(){
            var db = null;
            db = sqlitePlugin.openDatabase({name: 'damedashStudios.db',location: 1});
            alert('db test');
            db.transaction(function(transaction) {
                alert('db transaction');
                transaction.executeSql('CREATE TABLE IF NOT EXISTS damedashStudios_error_log (id integer primary key AUTOINCREMENT,source text , error text,code text,details text,device text)', [],
                function(tx, result) {
                    alert('table created')
                },
                function(error) {
                    alert(JSON.stringify(error))
              });
            });
    },

    onOnline: function(){
           // alert('online')
           /* var popup = document.getElementById('myPopup');
            popup.classList.toggle('show');*/
        },
     onOffline: function(){
            /*var popup = document.getElementById('myPopup');
            popup.classList.toggle('show');*/
        },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();