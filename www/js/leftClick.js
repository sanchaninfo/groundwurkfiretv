/*
     Module Name :
     File Name   :      leftClick.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      0.0.12 
     Created on  :      8 august 12:26 pm
     Last modified on:  27th Jan 2017
     Description :      functionality for left click.
     Organisation:      Peafowl inc.
*/
function leftClickFunction() {
    var isEpisodeThumbsVisible = $('#episodes').is(':visible');
    isThumbsListVisible = $('#thumbsList').is(':visible');
    var ismenuDiv = $('#menuDiv').is(':visible');
    var episodesDivVisible = $('#episodeMainDiv').is(':visible');
    var catVisible = $('#Catecomplete_' + countDownClickVar).is(':visible');
    var catSelectVisible = $('#categorySelectMain').is(':visible');
    var searchDivVisible = $('#divSearch').is(':visible');
    var searchBorder = $('#divimageBorderSearch').is(':visible');
     var videoRedirectIsVisible = $('#videoRedirect').is(':visible');
     var slideVisible = $('#slideShowImg').is(':visible');

    if (isThumbsListVisible) {
     
         $("#reg"+countDownClickVarLanding).addClass('snext');
       if(slickVal==countDownClickVarLanding){
        
        changeSlick = 1;
         var $carousel = $('.snext');
        $carousel.slick('slickPrev');
       }


    }
    else if(storeSliderVisible == 1){
        leftClick();
    }
  else if(slideShower == 1){
  
 $("#reg_Slide").addClass('snext');
       if(slickVal==countDownClickVarLanding){
        
        changeSlick = 1;
         var $carousel = $('.snext');
        $carousel.slick('slickPrev');
       }
}
     else if (catVisible) {
 
        categoryValueCount--;
        console.log('value count '+categoryValueCount)
        categoryDownClick--;
        flagRightClickCat = 0;
        rightAssetFound = 0;
        if (categoryValueCount < 0) {

            categoryValueCount = 0;
            categoryDownClick = 0;
            topMov = 0;
            rightMov = 0;
            var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
            //assetDetailsSplitCat(assetData);
             clearTimeout(timerCategory);  
                timerCategory = setTimeout(function() { 
                     //var assetValues = $('#assetid' + countDownClickVarLanding).val();
                     //assetDetailsSplitCat(assetData);
                      assetDetailsSplitCat(assetData);
                }, 1000);

            
            if(categoryVisibleChange==true) {
                $("#image1").hide().css({
                    marginLeft: "0px"
                });

                $("#descTxtCat").hide();
                //$("#menuDiv").fadeIn("slow");
                $("#menuDiv").animate({
                    left:"0px"   
                }, 500).show();

                 $("#CategroyDataDiv").animate({
                 right:"-25%"
                   
                }, 500);

                $("#Catecomplete_" + countDownClickVar).animate({
                    marginTop: "0px"
                }, 500);

                categoryVisibleChange = false; 
            }   
           
         
            

        

        } else if (categoryValueCount >= 0) {
            //categoryDownClick--;
            //categoryValueCount--;  

            rightMov = rightMov - marginValue;
            $("#image1").animate({
                marginLeft: rightMov + "px"
            }, 100);


            var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
            assetDetailsSplitCat(assetDataValues);
        }


    } else if (episodesDivVisible) {
         
        episodeAssetDownClick = 1;
        episodetopMov = 0;
        $(".borderEpisodes").fadeOut();
        $("#episodeList").addClass("episodeInactive");
        $("#episodeSeason_" + episodeDiv).animate({
            marginTop: "0px"
        }, 500);
        $("#episodeDescription").removeClass("episodeInactive");
    } else if (catSelectVisible) {
        
       // alert('catSelectCount left '+catSelectCount);
        if (catSelectCount > 1) {
            $("#" + catSelectCount).removeClass("active_sel");
            catSelectCount--;
            $("#" + catSelectCount).addClass("active_sel")
        }
        if (catSelectCount == 1) {
            $("#txtDescribe").html("Search for TV shows, movies, categories and music");
        } else if (catSelectCount == 2) {
            $("#txtDescribe").html("Browse categories");
        } else if (catSelectCount == 3) {
            $("#txtDescribe").html("Account settings");
        } else if (catSelectCount == 4) {
            $("#txtDescribe").html("Exit Dame Dash Studios");
        }
    } else if (searchDivVisible) {
         
        if (searchBorder) {
            if (searchRightClick > 0) {
                searchRightClick--;
                searchCountAsset--;
                var assetValSearch = $("#SearchList_" + searchCountAsset).attr("title");
                rightMovSearch = rightMovSearch - marginValueSearch;
                $("#divimageBorderSearch").animate({
                    marginLeft: rightMovSearch + "px"
                }, 50);
            } else {
                $("#" + keyPadCount + "g").addClass("searchCont_active");
                 $("#" + keyPadCount + "g").css({
                        color: 'black'
                    });
                $("#divimageBorderSearch").hide();
                $("#divsearchAssetCont").animate({
                    marginTop: "-150px"
                }, 500);
                topMovSearch = 150;
                searchCountAsset = 0;
            }

        } else {
            if (keyPadCount > 1) {
                if (keyPadCount == 1 && countDownKeyPad == 1) {

                }

                 else if (keyPadCount == leftClickSearchFun) {

                } else {
                     if(keyPadCount == 2){
                           $("#" + keyPadCount + "g").removeClass("searchContBackActive");
                    $("#" + keyPadCount + "g").css({
                        color: 'white'
                    });
                    keyPadCount--;
                    $("#" + keyPadCount + "g").addClass("searchCont_active_space");
                    $("#" + keyPadCount + "g").css({
                        color: 'black'
                    });
                    }
                    else{
                         $("#" + keyPadCount + "g").removeClass("searchCont_active");
                    $("#" + keyPadCount + "g").css({
                        color: 'white'
                    });
                    keyPadCount--;
                    $("#" + keyPadCount + "g").addClass("searchCont_active");
                    $("#" + keyPadCount + "g").css({
                        color: 'black'
                    });
                    }
                   
                    var assetVal = $("#" + keyPadCount + "g").attr("title");
                }
            }

        }
    }
     else if(videoRedirectIsVisible){
        
       if($( "#videFunc0" ).hasClass( "videFuncActive" )){
        
       }
       else if($( "#videFunc1" ).hasClass( "videFuncActive" )){
        
           if(nxtVidFunc > 0){   
             $("#child" + nxtVidFunc).removeClass("VEplaybtn_active");
            nxtVidFunc--;
            $("#child" + nxtVidFunc).addClass("VEplaybtn_active")
        }
       }
     
         
    }

     else if (isDescriptionDivVisible) {

        if ($("#episode_action3").hasClass("star_active1")) {
            var divCount = $('#starRateMainId').children('div').length;
            if (startRateCount >0 ) {
              
                $('#rating' + startRateCount).addClass('star').removeClass('star_active');
              startRateCount--;
            }

        }
    }
}