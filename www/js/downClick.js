/*
     Module Name :
     File Name   :      downClick.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :      
     license     :
     version     :      0.0.12. 
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November 2016
     Description :      down click functionality for fire tv remote control
     Organisation:      Peafowl inc.
*/
function downClickFunction() {
    isDescriptionDivVisible = $('#descriptionDivDescribe').is(':visible');
    isEpisode3Visible = $('#episode_action3').is(':visible');
    isThumbsListVisible = $('#thumbsList').is(':visible');
    var catVisible = $('#Catecomplete_' + countDownClickVar).is(':visible');
    var ismenuDiv = $('#menuDiv').is(':visible');
    var isEpisodeThumbsVisible = $('#episodeButtons').is(':visible');
    var isButtonShow = $('#buttonShow').is(':visible');
    var isEpisodeResumeVisible = $('#episode_actionResume').is(':visible');
    var episodesDivVisible = $('#episodeMainDiv').is(':visible');
    var videoDivVisible = $('#video_player').is(':visible');
    var videoStoreVisible = $('#player').is(':visible');
    var searchDivVisible = $('#divSearch').is(':visible');
    var searchBorder = $('#divimageBorderSearch').is(':visible');
     var settingsDiv = $('#divSettingsMain').is(':visible');
    var videoRedirectIsVisible = $('#videoRedirect').is(':visible');
    var slideShowVisible = $('#landingPageCarSlide').is(':visible');
    var slideShowMainVisible = $('#MainDivSlide').is(':visible');
    var videoStoreVisible = 
    downCount++;
    if (isEpisodeThumbsVisible) {

        if ($("#episodeDescription").hasClass("episodeInactive")) {
            episodeAssetDownClick++;
            var assetData = $("#Episode_" + episodeDiv + "_" + episodeAssetDownClick).attr("title");
            if (assetData != null) {
                episodetopMov = topmovdownclickEpisode + episodetopMov;
                $("#episodeList").load();
                $("#episodeSeason_" + episodeDiv).animate({
                    marginTop: -episodetopMov + "px"
                }, 500);
            } else {
                episodeAssetDownClick--;
            }
        } else {
            var episodesLen = seasons.length - 1;
            if (episodeDiv >= episodesLen) {
                episodeDiv = episodesLen
            } else {
                $("#season_" + episodeDiv).removeClass('catActive');
                //$("#Catecomplete_"+countDownClickVar).fadeOut();
                $("#episodeSeason_" + episodeDiv).hide();
                episodeDiv++;
                $("#season_" + episodeDiv).addClass('catActive');
                $("#episodeSeason_" + episodeDiv).show();
            }
        }
    }else if(storeSliderVisible == 1){
        downClick();
    }
     else if (isDescriptionDivVisible && isButtonShow) {

        descriptionDownlClick(isEpisode3Visible, isEpisodeResumeVisible, resumeEpisodeStat);
    }
    else if (slideShowVisible && slideShowMainVisible) {
      
       $('#imageBorder').addClass("borderLanding");
       $('#landingPageCarSlide').hide();
       $('#MainDivSlide').hide();
       var assetValues = $('#assetid' + countDownClickVarLanding).val();
       assetDetailsSplit(assetValues);
    } 
     else if (isThumbsListVisible || isDescriptionDivVisible) {
       console.log('downclick')
      //alert("CarouselItems.length "+carouselValue);
        var finalength = (carouselValue - 1) + mylistlength + recentlength  ;
        console.log('downclick value '+finalength);
        $(".slidern").removeClass("snext");
       
        if (countDownClickVarLanding >= finalength) {
            countDownClickVarLanding = finalength;
            slickVal = finalength;
        } else {
            
            $("#cate_" + countDownClickVarLanding).animate({
                marginTop: '-300px',
               
            }, 600,function() {
                clearTimeout(timer);  
                timer = setTimeout(function() { 
                     var assetValues = $('#assetid' + countDownClickVarLanding).val();
                     assetDetailsSplit(assetValues);
                }, 2000);
             });
            countDownClickVarLanding++;
            slickVal++;
           //var assetValues =  $("#reg" + countDownClickVarLanding +slideShowVal).attr("title")

       
            
        }
    }

    else if (ismenuDiv) {

        //Category menu hide/show data
        var finalength = CarouselItems.length - 1;
       
        if (countDownClickVar >= finalength) {
            countDownClickVar = finalength;
        } else {
            $("#Catedata_" + countDownClickVar).removeClass('cate_buttonactive');

            //$("#Catecomplete_"+countDownClickVar).fadeOut();
            $("#Catecomplete_" + countDownClickVar).hide();
            countDownClickVar++;
            $("#Catedata_" + countDownClickVar).addClass('cate_buttonactive');
            // $("#Catecomplete_"+countDownClickVar).fadeIn("slow","linear");
            $("#Catecomplete_" + countDownClickVar).show();
            var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
            assetDetailsSplitCat(assetData);
        }
    } else if (catVisible) {


        if (categoryDownClick == 1) {
            categoryDownClick = categoryDownClick + categoryValueCount + 4;

        } else {
            categoryDownClick = categoryDownClick + 5;

        }

        var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");

        if (assetData != null) {
            var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
            if (assetDataValues != null) {
                topMov = topmovdownclick + topMov;

        $("#Catecomplete_" + countDownClickVar).animate({
                marginTop: -topMov + "px"
            }, 600,function() {
                clearTimeout(timerCategory);  
                timerCategory = setTimeout(function() { 
                     //var assetValues = $('#assetid' + countDownClickVarLanding).val();
                     assetDetailsSplitCat(assetDataValues);
                }, 1000);

             });


               /* $("#Catecomplete_" + countDownClickVar).animate({
                    marginTop: -topMov + "px"
                }, 500);
                assetDetailsSplitCat(assetDataValues);*/
            }
        } else {
            var categoryDownClickTestVal = catListCount - 1;
             // alert('categoryDownClick '+categoryDownClick)
              var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClickTestVal).attr("title");
            if(assetData!= null && categoryValueCount == 4){
                //categoryDownClick = catListCount - 1;
                 catLastUpdatedVal = categoryDownClick - 5;
             categoryDownClick = catListCount - 1;
                categoryValueCount = (categoryDownClick - catLastUpdatedVal)-1;
                var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
                rightMov = 0;
                rightMov = marginValue * (categoryDownClick - catLastUpdatedVal);
                rightMov = rightMov - marginValue;
                //rightMov = rightMov/((categoryDownClick-1) - catLastUpdatedVal);
                 $("#image1").animate({
                    marginLeft: rightMov + "px"
                }, 100);
               // alert('assetData Value '+assetDataValues);
               topMov = topmovdownclick + topMov;
                $("#Catecomplete_" + countDownClickVar).animate({
                marginTop: -topMov + "px"
            }, 600,function() {
                clearTimeout(timerCategory);  
                timerCategory = setTimeout(function() { 
                     //var assetValues = $('#assetid' + countDownClickVarLanding).val();
                     assetDetailsSplitCat(assetDataValues);
                }, 1000);

             });
                catListCount++;
            }
            else{
                categoryDownClick = categoryDownClick - 5;
            }
            
        }
    } else if (videoDivVisible ) {
      
        videoFileId.setAttribute("controls", "controls")
        setTimeout(function() {
            videoFileId.removeAttribute("controls");
        }, 1000);
    }
     else if (searchDivVisible) {
       // alert('test');
        if (searchBorder) {
            searchCountAsset = searchCountAsset + 3;
            var assetValSearch = $("#SearchList_" + searchCountAsset).attr("title");
            if (assetValSearch != null) {
                topMovSearch = topMovSearchDim + topMovSearch;
                $("#divsearchAssetCont").animate({
                    marginTop: -topMovSearch + "px"
                }, 500);
            } else {
                searchCountAsset = searchCountAsset - 3;
            }
        } else {
            var activeId = $('.searchCont_active').attr('id');
            if(activeId == "divKeyBoardBackBtn"){

                  $("#divKeyBoardBackBtn").removeClass("searchCont_active");
                  $("#divKeyBoardBackBtn").css({
                            color: 'white'
                        });
                   keyPadCount = 2;
                $("#" + keyPadCount + "g").addClass("searchContBackActive");
                        $("#" + keyPadCount + "g").css({
                            color: 'black'
                        });
            }
            else{
                   if (countDownKeyPad < 6) {
                countDownKeyPad++
                if (keyPadValue < 38) {

                    keyPadValue = keyPadValue + 6;

                }
                if (keyPadCount == 2) {
                   
                    $("#" + keyPadCount + "g").removeClass("searchContBackActive");
                    $("#" + keyPadCount + "g").css({
                        color: 'white'
                    });
                    keyPadCount++;

                    $("#" + keyPadCount + "g").addClass("searchCont_active");
                    $("#" + keyPadCount + "g").css({
                        color: 'black'
                    });

                } else if (keyPadCount == 1) {
                    
                    $("#" + keyPadCount + "g").removeClass("searchCont_active_space");
                    $("#" + keyPadCount + "g").css({
                        color: 'white'
                    });
                    keyPadCount = 3;
                    $("#" + keyPadCount + "g").addClass("searchCont_active");
                    $("#" + keyPadCount + "g").css({
                        color: 'black'
                    });
                } else {
                    leftClickSearchFun = leftClickSearchFun + 6;

                    $("#" + keyPadCount + "g").removeClass("searchCont_active");
                    $("#" + keyPadCount + "g").css({
                        color: 'white'
                    });
                    keyPadCount = keyPadCount + 6;
                    $("#" + keyPadCount + "g").addClass("searchCont_active");
                    $("#" + keyPadCount + "g").css({
                        color: 'black'
                    });
                }
            }
            }
        }

    }else if(settingsDiv){
        if(settingsClick < 1){
                $("#divSettingsMainBackBtn").removeClass("src_nav_active");
                 $("#gbBackBrowse").removeClass("gb_act");
                settingsClick ++;
                $("#divSettingsMainExitBtn").addClass("src_nav_active");
                $("#gbBackBrowse").addClass("gb_btn");
        }
    }
     else if(videoRedirectIsVisible){
     if(videoRedirectActive<1){
       
         $("#videFunc" + videoRedirectActive).removeClass("videFuncActive");
            videoRedirectActive++;
        $("#videFunc" + videoRedirectActive).addClass("videFuncActive"); 
         $("#backVidRet").removeClass("VEbackbtn_act"); 
          
     }   
    }

}