function onErrorLogInsertionToServer(source,error,code,details,device) {
   
    var networkCheck = checkNetworkConnectivity();
    if (networkCheck == 1) {
                        $.ajax({
                            type: "POST",
                            url: URL_LINK + "/insertLog",
                            data: JSON.stringify({
                                "insertLog": {
                                    "source": source,
                                    "error": error,
                                    "code": code,
                                    "details": details,
                                    "device": device
                                }
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(data) {
                               
                            },
                            error: function(jqXHR, textStatus, errorThrown) {

                            },
                            complete: function() {
                            }
                        });
    }
    else{
     onDatabaseInsertion(source,error,code,details,device);
    }
}