function timetosecond(time){
    var hmis = time;  
    var a = hmis.split(':'); 
    var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
    var sec = (seconds*1000);

    return sec;
}

function timetosecondonly(time){
    var hmis = time;  
    var a = hmis.split(':'); 
    var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
    return seconds;
}

function registrationFunction() {

    var Store = window.localStorage;

    //Check verification exist or not   
    setTimeout(getRegTimeOut, 3000);
    function getRegTimeOut() {
        if (Store.getItem("codeValue") == null) {

            createDataBase();
        }
        else {
            getRegisteredData(false);
        }
    }
}



window.addEventListener("load", function() {
    window.addEventListener("popstate", function() {
        if (window.history.state !== "backhandler") {
            window.history.pushState("backhandler", null, null);
         
            if(productspage==true){

               
                 PlayPause();
                 productspage = false;
                 homepage = true;
                 homepagebottom = true;
                 homepageright = true;
                 homepageleft = false;
                 productpageleft = true;
                 productpageright = false;
                 countSize = 0;
                 countDownClickProduct=0;
                 colorClick = 0;
                 quantityCount = 1;
                  addressMargintop = 25;

               //  getproducts();
               /* $(".slider"+v+carousel_value).unslick();
                $(".slider-for"+v+carousel_value).unslick();
                $(".slider"+v+carousel_value).slick('reinit');
                $(".slider-for"+v+carousel_value).slick('reinit');*/

            /*$(".slider"+v+carousel_value).slick('resize');
            $(".slider-for"+v+carousel_value).slick('resize');
*/
                 $("#ProductDetails").hide();
              //   $("#HomeDiv").show();
                 $('#HomeDiv').css("visibility", "visible");
                 $('.snb').css("display", "block");
                 
                 $('#showprod').css("visibility", "visible");
                 $('#preview_Thumb').empty();
                 $('#sizeDimen').empty();
                 $('#preview_Color').empty();

                $("#continue_checkout").removeClass("continueactive").show()
                $("#confirm_checkout").removeClass("continueactive").hide();
                $(".prd_aro").removeClass("bgactivecolor bgblackactivecolor");
                $(".prz_txt").removeClass("bgactivecolor bgblackactivecolor");
                $(".prd_pz").removeClass("borderactivecolor borderblackactivecolor");

                $("#selectedColor").css("background-color","");
                $("#selectedSize").html("");
                $("#selectedQty").html("");

                // $("#preview_Thumb").html();
                
                 $("#qty1").html(quantityCount);

                 
            }
            else if(addresspage==true){
               
                 $("#addressDetailsDiv").hide();
                 $("#ProductDetails").show();

                 $("#continue_checkout").removeClass("continueactive").show()
                $("#confirm_checkout").removeClass("continueactive").hide();
                $(".prd_aro").removeClass("bgactivecolor");
                $(".prz_txt").removeClass("bgactivecolor");
                $(".prd_pz").removeClass("borderactivecolor");
                $(".prd_pz").removeClass("borderblackactivecolor"); 
                 productspage = true;
                 addresspage = false;
                 productpageleft = true;
                 productpageright = false;

                /* address_defaultindex = addresses[0].address.findIndex(function(element) {
                  return element.defaultAddress == true;
                });


                  if(address_defaultindex==0){
                addressMargintopdefault = addressMargintop*address_defaultindex;
                }else {
                addressMargintopdefault = addressMargintop*(address_defaultindex-1);
                }
                $('#addressbook').animate({ marginTop: '-'+addressMargintopdefault+'vh'}, 500);
                */
            }
            else{
               window.location.href = 'index.html';
            }
        }
    });
    window.history.pushState("backhandler", null, null);
});

$(document).ready(function() {
storeVisFlag = 1;
var storage = window.localStorage;
userDeviceId = storage.getItem('userIdVal');
//userDeviceId = "58f48e23a1f59c490c100bb3";
$.ajax({
    type: "GET",
    url: mainUrl + appDataUrl,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (data) {
      appDataFinal = data;
      $('#appName').html(appDataFinal.siteTitle);
      $('#appMainUrl').html(mainUrl);
      var applogofinal = ul_ext+'/'+appDataFinal.siteLogo;
      $('#appLogo').attr("src",applogofinal);
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log("error ---");
    }
});

//registrationFunction();
getassetdata();

});


var curint = setInterval(function(){ 
    if(videoLinks!=undefined){
        showhide();
    }
}, 1000);

function datFormat(dateObject) {
        var d = new Date(dateObject);

        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        var months = new Array(12);
        months[1] = "Jan";
        months[2] = "Feb";
        months[3] = "Mar";
        months[4] = "Apr";
        months[5] = "May";
        months[6] = "Jun";
        months[7] = "Jul";
        months[8] = "Aug";
        months[9] = "Sep";
        months[10] = "Oct";
        months[11] = "Nov";
        months[12] = "Dec";


        if (day < 10) {
            day = "0" + day;
        }
        
        var date = day + " " + months[month] + " " + year;

    return date;
}


function getassetdata(){

$.ajax({
    type: "GET",
    url: mainUrl + videoDataUrl,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (data) {
        
        videoLinks = data;
        HRLURL = videoLinks.dataa[v].videoURL;
       // HRLURL = "https://s3.amazonaws.com/peafowl/puma-store-dev/puma.mp4";
      //  HRLURL = "http://localhost:5000/1.mp4";
         HRLPOSTER = videoLinks.dataa[v].posterURL;
         carleng = videoLinks.dataa.length;
         hms = timetosecondonly(videoLinks.dataa[v].duration);
         $("#video_player").attr('src',HRLURL); 
         $("#video_player").attr('poster',HRLPOSTER);   
         $("#player").css("z-index","0"); 
        
         $("#slider").addClass("slider"+v+carousel_value);
         $("#sliderfor").addClass("slider-for"+v+carousel_value);

         $(".slider"+v+carousel_value).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            centerMode: true,
            asNavFor: ".slider-for"+v+carousel_value,
            infinite: true
        });

        $(".slider-for"+v+carousel_value).slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: ".slider"+v+carousel_value,
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: true
        }); 
         getproducts();
         fetchaddress(); 
        
    console.log("data == ");
    },
    error: function (jqXHR, textStatus, errorThrown) {
        // alert("  ");
        console.log("error ---");
    }
});
}




function fetchaddress(){

  // alert('sdfdf storingIn' +storingIn);
//storingIn =  "58e36ddf1f23feff3631d37c";


var fetch_address  = { "fetchAddressStore": { "user_id": userDeviceId }}
$.ajax({
        url: mainUrl + fetchAddrressUrl,
        type: "POST",
        crossDomain: true,
        data: JSON.stringify(fetch_address),
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
           addresses = data;
            if (data.length != 0)   // address is present.
            {
                
                $("#address_Count").html("("+data[0].address.length+")");
                for (var k = 0; k < data[0].address.length; ++k) {
                        if(data[0].address[k].defaultAddress==true){
                           var classaddressact = 'adr_wrp_act';
                        }else {
                          var classaddressact= '';
                        }

                    $("#addressbook").append('<div id="adr_wrp'+k+'" class="adr_wrp '+classaddressact+'"><div class="col-md-6"><h3>Billing</h3><p>' + data[0].address[k].billingAddress.streetAddress + " " + data[0].address[k].billingAddress.extendedAddress + "," + data[0].address[k].billingAddress.locality + "," + data[0].address[k].billingAddress.region + "," + data[0].address[k].billingAddress.postalCode + '</p></div><div class="col-md-6"><h3>Shipping</h3><p>' + data[0].address[k].shippingAddress.streetAddress + " " + data[0].address[k].shippingAddress.extendedAddress + "," + data[0].address[k].shippingAddress.locality + "," + data[0].address[k].shippingAddress.region + "," + data[0].address[k].shippingAddress.postalCode + '</p></div><div class="clearfix"></div></div>');
                  
                }

             address_defaultindex = addresses[0].address.findIndex(function(element) {
                  return element.defaultAddress == true;
                });

            }else {
                    $("#addressbooknew").empty().append('<br/><strong>Please provide Billing and Shipping details.</strong><br/><strong>Go to '+mainUrl+'</strong><br/><strong>My Account</strong><br/><strong>Add Billing details</strong><div class="adr_cn"><div class="adrr_btn btnact" id="refbut" >RESUME</div></div>');

                }
            },
        error: function (jqXHR, textStatus, errorThrown) {

        }
});
}




function bvlink() {
    if(v==carleng-1){
        v = 0;
        cl = 0;
        carousel_value=0;
        hms = timetosecondonly(videoLinks.dataa[v].duration);
        HRLURL = videoLinks.dataa[v].videoURL;
        HRLPOSTER = videoLinks.dataa[v].posterURL;
    }else {
        v = v+1;
        cl = 0;
        carousel_value=0;
        hms = timetosecondonly(videoLinks.dataa[v].duration);
        HRLURL = videoLinks.dataa[v].videoURL;
        HRLPOSTER = videoLinks.dataa[v].posterURL;
    }
    videobl(HRLURL,HRLPOSTER);
}

function videobl(HRLURL,HRLPOSTER) {
    $("#video_player").attr('src',HRLURL); 
    $("#video_player").attr('poster',HRLPOSTER);  
}



function showhide() {
    if(getcur>hms){
        bvlink();
    }
setslider++;
//console.log('setslider '+setslider);
if(setslidershow < setslider ){
    slidershow = false;
}
player = document.getElementById("video_player");
getcur = player.currentTime;
console.log("getcur "+getcur);

if (parseInt(getcur) > 1) {
 $("#loaderDiv").hide();
}

    var caroleng = videoLinks.dataa[v].carousels.length;

        if(cl<=(caroleng-1))
        {
            cl = cl;
        }else{
             cl = 0;
        }

    var startAt = timetosecondonly(videoLinks.dataa[v].carousels[cl].startAt);
    var stopAt = timetosecondonly(videoLinks.dataa[v].carousels[cl].stopAt);

    if (getcur > startAt && getcur < stopAt){
        $('#shopthecollection').css("visibility", "visible"); 
        $('#closhop').css("visibility", "visible"); 
    }else {
       if(slidershow==false){ 
        $('#shopthecollection').css("visibility", "hidden");
        $('#closhop').css("visibility", "hidden");
        $("#shopthecollection").removeClass("hideshop");
        $("#closhop").removeClass("hideshop");
        $('#showprod').css("visibility", "hidden");
         $('#cloprod').css("display", "none");
        }
    }
   // console.log('slidershow '+slidershow);
    if(slidershow==false){
        carousel_value = videoLinks.dataa[v].carousels[cl].carousel_value;   
    }
   
    if(getcur > stopAt){
      cl = cl+1;  
     // carousel_value = videoLinks.dataa[v].carousels[cl].carousel_value; 
        
    }
    if(k != v || kl != carousel_value){
        console.log("kl "+ kl +' carousel_value ' +carousel_value+' k'+k+' v'+v);
        $("#shopthecollection").addClass("btnact");
        $("#closhop").removeClass("btnact");
        
        getproducts(); 
    }
    console.log("cl v "+ v +' ' +cl);

}


function getproducts(){
    var productsData = videoLinks.dataa[v].carousels[carousel_value].products;
    var prolength =videoLinks.dataa[v].carousels[carousel_value].products.length;
    var cartlogo = videoLinks.dataa[v].carousels[carousel_value].carouselLogo;

//console.log('productsData'+ JSON.stringify(productsData));
//console.log("slider k kl "+k+kl);
 //console.log('carousel_value '+carousel_value);

$(".slider"+k+kl).slick('unslick').empty(); 

$("#slider").removeClass("slider"+k+kl);

$(".slider-for"+k+kl).slick('unslick').empty(); 
$("#sliderfor").removeClass("slider-for"+k+kl);

$("#slider").addClass("slider"+v+carousel_value);
$("#sliderfor").addClass("slider-for"+v+carousel_value);
  
console.log("slider"+v+carousel_value);



    for (var i = 0; i < prolength; ++i) {
    $(".slider"+v+carousel_value).append('<div id="'+ productsData[i].id +'"><img  width="90%" class="liImage" alt="'+ productsData[i].id +'" src="' + ul_ext + productsData[i].images[0].thumb + '"  id="img' + parseInt(i + 1) +'"   ></div>');
    $(".slider-for"+v+carousel_value).append('<div><div class="logo" id="logo1" ><img  src="'+cartlogo+'" /></div><ul><li id="sbid">"' + productsData[i].id + '"</li><li id="sbdesc">' + productsData[i].title + '</li><li id="sbprice">Our Price: <span class="pricered">$' + productsData[i].price + '</span></li></ul><div class="button pointer clickbuy" alt="'+ productsData[i].id +'">BUY NOW</div></div>');

        
        $('#prod_Count').html(prolength);
   
        if (parseInt(i + 1) == prolength) {

            $(".slider-for"+v+carousel_value).slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              centerMode: true,
              asNavFor: ".slider"+v+carousel_value,
              infinite: true
            });

            $(".slider"+v+carousel_value).slick({
              slidesToShow: 6,
              slidesToScroll: 1,
              asNavFor: ".slider-for"+v+carousel_value,
              dots: false,
              centerMode: false,
              focusOnSelect: false,
              infinite: true
            });

            $(".slider"+v+carousel_value).addClass("homeactive");

            k = v;
            kl = carousel_value;
        }

         
    }
}


function seekRight() {
    var playTime = parseFloat(player.getCurrentTime() + 10);
    if (playTime >= player.getDuration()) {
        playTime = player.getDuration();
    }
    player.seek(playTime);
}
function seekLeft() {
    var playTime = parseFloat(player.getCurrentTime() - 10);
    if (playTime <= 0) {
        playTime = 0;
    }
    player.seek(playTime);
}

function PlayPause() {
    if (player.paused == false) {
        player.pause();
    } else {     
        player.play();
    }
}

function volumeup(){ 
    if(vol>=100){
        vol = parseInt(vol);
    }else{
        vol = parseInt(vol)+5;
    }
    $('#slidervol').css({
        'backgroundSize': (vol - 0) * 100 / (100 - 0) + '% 100%'
        });
    if(vol<6){
        $("#voloff").attr('src', 'img/off.png');
    }else {
        $("#voloff").attr('src', 'img/on.png');
    }
    player.setVolume(vol);
    $('#slidervol').val(vol);
}

function volumedown(){
    if(vol<=5){
        vol = parseInt(vol);
    }else{
        vol = parseInt(vol)-5;
    }

    $('#slidervol').css({
        'backgroundSize': (vol - 0) * 100 / (100 - 0) + '% 100%'
        });
    if(vol<6){
        $("#voloff").attr('src', 'img/off.png');
    }else {
        $("#voloff").attr('src', 'img/on.png');
    }
    player.setVolume(vol);
    $('#slidervol').val(vol);
}

function getGoal(id) {
    
    for(var d=0;d<videoLinks.dataa.length;d++){
        
        var clength = videoLinks.dataa[d].carousels.length;
        for(var cli=0;cli<clength;cli++){
           
            var caro = videoLinks.dataa[d].carousels[cli].products; 
            
            for(var c=0;c<caro.length;c++){
               
                if(caro[c].id == id)
                return _.find(caro, function(goal) {
                 
                    return goal.id == id;
                });
            }
        }
    }
}

function productdetails(product){
    console.log('product final '+JSON.stringify(product));
   
    var previewImg = ul_ext+product.images[0].large;
    var product_imglength = product.images.length;
    var product_colorlength = product.colors.length;
    var product_sizelength = product.sizes.length;
    console.log('img '+previewImg);
    $('#preview_Img').attr("src",previewImg);
    $('#preview_Desc').html(product.description);
    $('#preview_Title').html(product.title);
    $('#preview_Sku').html("SKU - "+product.sku);
    $('#preview_Price').html('$'+product.clearancePrice);
    

     for (var i = 0; i < product_imglength; ++i) {
        if(i==0){
           var classact = 'act';
        }else {
          var classact= '';
        }
        $("#preview_Thumb").append('<li id="thumb'+i+'"  class="'+classact+' border"><img src="'+ul_ext+product.images[i].thumb+'"></li>');
     }
     console.log('product_colorlength '+product_colorlength);
     for (var c = 0; c < product_colorlength; ++c) {
        if(c==0){
           var classcoloract = 'act';
        }else {
          var classcoloract= '';
        }
        $("#preview_Color").append('<span id="color'+c+'" class="'+classcoloract+' colorborder" style="background-color:'+product.colors[c]+'"></span>');
     }
   

    for (var s = 0; s < product_sizelength; ++s) {
        $("#sizeDimen").append('<span id="sizeid'+s+'" class="sizeborder" >'+product.sizes[s]+'</span>');
     }
     

    $("#ProductDetails").show();
   // $("#HomeDiv").hide();
   $("#HomeDiv").css("visibility", "hidden");
   $('.snb').css("display", "none");
    $('#showprod').css("visibility", "hidden");
    
}

function onThumbClick(large,imgid) {
    var previewImg = ul_ext+large;

    $('.border').removeClass("act");
    $('#preview_Img').attr("src",previewImg);
    $('#thumb'+imgid).addClass("act");
}


function payWithCVV(bill) {
 
    var storage = window.localStorage;
    var ship_address = bill.shippingAddress;
    var bill_address = bill.billingAddress;
    var userNameValue = storage.getItem("fullNameValue");
    var userIdValue = storage.getItem("userIdVal");
    var customerIdValue = storage.getItem("customer_idValue");
    var userEmail = storage.getItem("emailValue");
    
    $.ajax({
        type: "POST",
        url: mainUrl + payWithCVVUrl,

        data: JSON.stringify({ "payWithCVV": { "email": userEmail, "billingAddress": bill_address, "shippingAddress": ship_address, "userName": userNameValue, "productName": selectedProductName, "productPrice": selectedProductPrice, "productColor": selectedProductColor, "productSize": selectedProductSize, "productQuantity": selectedProductQty, "productDeliveryCharge": "Free", "userPhoneNumber": "", "productImage": selectedProductImage, "productId": selectedProductId, "userId": userIdValue, "customerId": customerIdValue,"clearancePrice": selectedUnitPrice } }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          
           var OS_orderDate =datFormat(data.order.orderDate);
          

          $('#OS_OrderId').html(data.order.orderId);
          $('#OS_OrderDate').html(OS_orderDate);
          $('#OS_OrderProduct').html(data.order.productName);
          $("#OS_OrderImg").attr('src', data.order.productImage);
          $('#OS_OrderPrice').html(data.order.productPrice);
        //  $('#OS_OrderColor').html(data.order.productColor);

          $("#OS_OrderColor span").css("background-color",data.order.productColor);

          $('#OS_OrderSize').html(data.order.productSize);
          $('#OS_OrderQty').html(data.order.productQuantity);
          $('#OS_OrderBilling').html(data.order.deliveryAddressLine1+''+data.order.deliveryAddressLine2+''+data.order.deliveryAddressLine3);

          $("#orderblock").show();

          
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(" --  --");
        }
    });
}
