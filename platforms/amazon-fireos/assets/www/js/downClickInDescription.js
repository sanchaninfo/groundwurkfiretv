/*
     Module Name :
     File Name   :      downClickInDescription.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0 
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      navigation of buttons in description page for down arrow click
     Organisation:      Peafowl inc.
*/
function descriptionDownlClick(episodeButtonVisible, resumeButtonVisible, stat) {

    if (episodeButtonVisible && stat == 0) {
        if (flagButtonMovement == 0) {
            if (episodeClick < 3) {


                if (episodeClick == 1) {
                    $("#episode_action" + episodeClick).removeClass("ps1_landingPage_active");
                    $("#episode_action" + episodeClick).css({
                        color: 'white'
                    });
                }

                if (episodeClick == 2) {
                    if (flagEnterVariable == 1) {
                        $("#episode_action" + episodeClick).removeClass("addlist_active1");
                        $("#episode_action" + episodeClick).css({
                            color: 'white'
                        });
                        $("#episode_action" + episodeClick).removeClass("addlist_active");
                        $("#episode_action" + episodeClick).css({
                            color: 'white'
                        });
                        $("#episode_action" + episodeClick).addClass("addlist1");
                        $("#episode_action" + episodeClick).css({
                            color: 'white'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action" + episodeClick).removeClass("addlist_active");
                                $("#episode_action" + episodeClick).css({
                                    color: 'white'
                                });
                            } else {
                                $("#episode_action" + episodeClick).removeClass("addlist_active1");
                                $("#episode_action" + episodeClick).css({
                                    color: 'white'
                                });
                            }

                        } else {
                            $("#episode_action" + episodeClick).removeClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'white'
                            });
                        }

                    }
                }


                episodeClick++;
                if (episodeClick == 3) {
                    //$("#episode_action"+episodeClick).removeClass("episodes");
                    $("#episode_action" + episodeClick).addClass("episode_active1");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                }
                if (episodeClick == 2) {
                    if (flagEnterVariable == 1) {
                        $("#episode_action" + episodeClick).addClass("addlist_active1");
                        $("#episode_action" + episodeClick).css({
                            color: 'black'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action" + episodeClick).addClass("addlist_active");
                                $("#episode_action" + episodeClick).css({
                                    color: 'black'
                                });
                            } else {
                                $("#episode_action" + episodeClick).addClass("addlist_active1");
                                $("#episode_action" + episodeClick).css({
                                    color: 'black'
                                });
                            }

                        } else {

                            $("#episode_action" + episodeClick).addClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        }
                    }

                }

               
            }
        }

    } else if (resumeButtonVisible && stat == 0) {
        if (flagButtonMovement == 0) {
            if (episodeClick < 3) {
                episodeClick++;

                if (episodeClick == 3) {
                    if (flagEnterVariable == 1) {
                        $("#episode_action2").addClass("addlist_active1");
                        $("#episode_action2").css({
                            color: 'black'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action2").addClass("addlist_active");
                                $("#episode_action2").css({
                                    color: 'black'
                                });
                            } else {
                                $("#episode_action2").addClass("addlist_active1");
                                $("#episode_action2").css({
                                    color: 'black'
                                });
                            }

                        } else {

                            $("#episode_action2").addClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'black'
                            });
                        }
                    }
                }
                if (episodeClick == 2) {
                    $("#episode_action1").addClass("ps1_landingPage_active");
                    $("#episode_action1").css({
                        color: 'black'
                    });

                }

                episodeClick--;

                if (episodeClick == 1) {
                    $("#episode_actionResume").removeClass("ps1_landingPageResume_active");
                    $("#episode_actionResume").css({
                        color: 'white'
                    });
                }

                if (episodeClick == 2) {
                    $("#episode_action1").removeClass("ps1_landingPage_active");
                    $("#episode_action1").css({
                        color: 'white'
                    });
                }

                episodeClick++;
            }
        }
    } else if ((episodeButtonVisible == true && resumeButtonVisible == true) && stat == 1) {
        if (flagButtonMovement == 0) {
            if (episodeClick < 4) {
                episodeClick++;
                if (episodeClick == 4) {
                    //$("#episode_action"+episodeClick).removeClass("episodes");
                    $("#episode_action3").addClass("episode_active1");
                    $("#episode_action3").css({
                        color: 'black'
                    });
                }
                if (episodeClick == 3) {
                    if (flagEnterVariable == 1) {
                        $("#episode_action2").addClass("addlist_active1");
                        $("#episode_action2").css({
                            color: 'black'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action2").addClass("addlist_active");
                                $("#episode_action2").css({
                                    color: 'black'
                                });
                            } else {
                                $("#episode_action2").addClass("addlist_active1");
                                $("#episode_action2").css({
                                    color: 'black'
                                });
                            }

                        } else {

                            $("#episode_action2").addClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'black'
                            });
                        }
                    }
                }
                if (episodeClick == 2) {
                    $("#episode_action1").addClass("ps1_landingPage_active");
                    $("#episode_action1").css({
                        color: 'black'
                    });

                }

                episodeClick--;

                if (episodeClick == 1) {
                    $("#episode_actionResume").removeClass("ps1_landingPageResume_active");
                    $("#episode_actionResume").css({
                        color: 'white'
                    });
                }

                if (episodeClick == 2) {
                    $("#episode_action1").removeClass("ps1_landingPage_active");
                    $("#episode_action1").css({
                        color: 'white'
                    });
                }
                if (episodeClick == 3) {
                    if (flagEnterVariable == 1) {
                        $("#episode_action2").removeClass("addlist_active1");
                        $("#episode_action2").css({
                            color: 'white'
                        });
                        $("#episode_action2").removeClass("addlist_active");
                        $("#episode_action2").css({
                            color: 'white'
                        });
                        $("#episode_action2").addClass("addlist1");
                        $("#episode_action2").css({
                            color: 'white'
                        });
                    } else {
                        if (flagDivVal == 1) {
                            if (flagRemVal == 1) {
                                $("#episode_action2").removeClass("addlist_active");
                                $("#episode_action2").css({
                                    color: 'white'
                                });
                            } else {
                                $("#episode_action2").removeClass("addlist_active1");
                                $("#episode_action2").css({
                                    color: 'white'
                                });
                            }

                        } else {
                            $("#episode_action2").removeClass("addlist_active");
                            $("#episode_action2").css({
                                color: 'white'
                            });
                        }

                    }
                }

                episodeClick++;
            }
        }
    } else {

        if (episodeClick < 2) {
            episodeClick++;


            if (episodeClick == 2) {
                if (flagEnterVariable == 1) {
                    $("#episode_action" + episodeClick).addClass("addlist_active1");
                    $("#episode_action" + episodeClick).css({
                        color: 'black'
                    });
                } else {
                    if (flagDivVal == 1) {
                        if (flagRemVal == 1) {
                            $("#episode_action" + episodeClick).addClass("addlist_active");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        } else {
                            $("#episode_action" + episodeClick).addClass("addlist_active1");
                            $("#episode_action" + episodeClick).css({
                                color: 'black'
                            });
                        }

                    } else {

                        $("#episode_action" + episodeClick).addClass("addlist_active");
                        $("#episode_action" + episodeClick).css({
                            color: 'black'
                        });
                    }
                }

            }

            episodeClick--;

            if (episodeClick == 1) {
                $("#episode_action" + episodeClick).removeClass("ps1_landingPage_active");
                $("#episode_action" + episodeClick).css({
                    color: 'white'
                });
            }


            episodeClick++;
        }
    }
}