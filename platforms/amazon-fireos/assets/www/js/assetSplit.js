/*
     Module Name :
     File Name   :      assetSplit.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0 // written by sachin for present.
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      Spliting asset details to display them on landing div and category div on navigating 
                        to particular asset.
     Organisation:      Peafowl inc.
  */
function assetDetailsSplit(assetValues) {
  console.log('asset values '+JSON.stringify(assetValues))
    var assetSplitArray = []
    assetSplitArray = assetValues.split("|");
    console.log('asset spli '+assetSplitArray.length)
 if(assetSplitArray.length > 1){
 
    if (assetSplitArray[1] != null) {
        $("#movieTitle").html(assetSplitArray[1]);
    }
    //if (assetSplitArray[3] != null && assetSplitArray[5] != null)
    //releaseDateAndDurationInLanding(assetSplitArray[3], assetSplitArray[5]);
    
    $('#landing_BackgroundImage').fadeOut(300, function() {
        
       /* bgimage = new Image();

        if (assetSplitArray[4] != null)
        bgimage.src = assetSplitArray[4];    */   
        bgimage = new Image();
        var imgUrl = "https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/58f9dde2-bc14-483e-94bc-ef21627c32d2.png";
        if (assetSplitArray[4] == null || assetSplitArray[4] ==""){
           
            
             bgimage.src = imgUrl; 
        }
        else{
           
            bgimage.src = assetSplitArray[4];
        }
        
        $(bgimage).load(function(){
          
            //$("#landingPage").append('<div class="landing_bgimage" id="landing_BackgroundImage"></div>');
         if(assetSplitArray[4] == "" || assetSplitArray[4] == null){
           
            $("#landing_BackgroundImage").css("background-image","url("+imgUrl+")").fadeIn(300);
                
           }else{
              
            $("#landing_BackgroundImage").css("background-image","url("+assetSplitArray[4]+")").fadeIn(300);
           }
           
          
         }); 

        
    });
    var length = 100;
    var trimmedString = assetSplitArray[2].length > length ?
        assetSplitArray[2].substring(0, length - 3) + "..." :
        assetSplitArray[2];
    if (assetSplitArray[2].length == 0) {
        $('#synop').html(trimmedString.replace(/[^a-zA-Z 0-9]+/g, ""));
    } else {
        $('#synop').html(trimmedString.replace(/[^a-zA-Z 0-9]+/g, "") + '...');
    }

    $('#rating').html('<div id = "rate1" class="ratingStar"></div><div id = "rate2" class="ratingStar"></div><div id = "rate3" class="ratingStar"></div><div id = "rate4" class="ratingStar"></div><div id = "rate5" class="ratingStar"></div>');
    $('#imageBorder').addClass("borderLanding")
 }
 else{
   
    $("#movieTitle").html("");
    $('#synop').html("");
    $('#rating').html('');
    $('#movieDuration').html("");
    $('#movieRelease').html(""); 
   // $('#imageBorder').addClass("borderLanding")
    $('#landing_BackgroundImage').fadeOut(300, function() {
        
        bgimage = new Image();

            var imgUrl = "https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/58f9dde2-bc14-483e-94bc-ef21627c32d2.png";
        if (assetSplitArray[0] == null || assetSplitArray[0] ==""){
           
             
             bgimage.src = imgUrl; 
        }
        else{
           
            bgimage.src = assetValues;
        }
       /* if (assetSplitArray[0] != null)
        bgimage.src = assetValues;   */    
        
        $(bgimage).load(function(){
            if(assetValues !="" || assetValues !=null){
                
                 $("#landing_BackgroundImage").css("background-image","url("+assetValues+")").fadeIn(300); 
            }
           else{
             
            $("#landing_BackgroundImage").css("background-image","url("+imgUrl+")").fadeIn(300);
           }
          
         }); 

        
    });
 }

}


function assetDetailsSplitCat(assetValues) {
    
    var assetSplitArray = []
    assetSplitArray = assetValues.split("|");
    $("#movieTitleCat").html(assetSplitArray[1]);
    //releaseDateAndDurationInCat(assetSplitArray[3], assetSplitArray[5]);

 $('#catBackgroundImage').fadeOut(300, function() {
        
        bgimageCat = new Image();      
       // bgimageCat.src = assetSplitArray[4];

        var imgUrl = "https://d27f70r2nf2lxy.cloudfront.net/907903041SLYW/58f9dde2-bc14-483e-94bc-ef21627c32d2.png";
        if (assetSplitArray[4] == null || assetSplitArray[4] ==""){
           
             
             bgimageCat.src = imgUrl; 
        }
        else{
           
           bgimageCat.src = assetSplitArray[4];
        }       
        
        $(bgimageCat).load(function(){
            if(assetSplitArray[4] =="" || assetSplitArray[4] ==null){
              
                // $("#catBackgroundImage").css("background-image","url("+assetSplitArray[4]+")").fadeIn(300); 

                    $("#catBackgroundImage").css("background-image","url("+imgUrl+")").fadeIn(300);
            }
           else{
            
           // $("#catBackgroundImage").css("background-image","url("+imgUrl+")").fadeIn(300); 


            $("#catBackgroundImage").css("background-image","url("+assetSplitArray[4]+")").fadeIn(300); 
           }
         }); 
    });

   /* $('#catBackgroundImage').fadeOut(300, function() {
        $('#catBackgroundImage').css({
            'background-image': "url('" + assetSplitArray[4] + "')"
        });
        $('#catBackgroundImage').fadeIn(300);
    });*/
    $('#synopCat').html(assetSplitArray[2].replace(/[^a-zA-Z 0-9]+/g, ""));
}

function assetSlideShow() {
    
    
//var status = ["ready", "steady", "go"];

var counter;
(function recurse(counter) {
    var color = slideImage[counter];
    var imgName = slideImageName[counter];
/*$('#landing_BackgroundImageCar').fadeOut(5000, function() {
    $("#landing_BackgroundImageCar").css("background-image","url("+color+")").fadeIn(1000); 
    $("#movieTitleSlide").html(imgName).fadeIn(1000);
    }); */
    
    delete slideImage[counter];
    delete slideImageName[counter];
    slideImage.push(color);
    slideImageName.push(imgName);
    sliderTimer = setTimeout(function() {
        recurse(counter + 1);
    }, 5000);
})(0);
}