/*
     Module Name :
     File Name   :      releaseDateAndDuration.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      0.0.12 
     Created on  :      8 august 12:26 pm
     Last modified on:  30 November
     Description :      converting release date and time into hours and minutes.
     Organisation:      Peafowl inc.
*/

function releaseDateAndDurationInLanding(duration,listYear)
{
               var timeSplitArray = []
               timeSplitArray = timeConversion(duration).split(",");
               if(timeSplitArray[0] == 0)
               {
                 $('#movieDuration').html(timeSplitArray[1]+"m");
                 $('#movieRelease').html(yearOfRelease(listYear)); 
               }
               else
               {
                 $('#movieDuration').html(timeSplitArray[0]+"h "+timeSplitArray[1]+"m");
                 $('#movieRelease').html(yearOfRelease(listYear)); 
               }    
}

function releaseDateAndDurationInCat(duration,listYear)
{
                var timeSplitArray = []
               timeSplitArray = timeConversion(duration).split(",");
               if(timeSplitArray[0] == 0)
               {
                 $('#movieDurationCat').html(timeSplitArray[1]+"m");
                 $('#movieReleaseCat').html(yearOfRelease(listYear)); 
               }
               else
               {
                 $('#movieDurationCat').html(timeSplitArray[0]+"h "+timeSplitArray[1]+"m");
                 $('#movieReleaseCat').html(yearOfRelease(listYear)); 
               }    
}

