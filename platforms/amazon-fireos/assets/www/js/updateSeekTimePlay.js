/*
     Module Name :
     File Name   :      enterFunction.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      0.0.12 // 
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      functionality for video playing and updating the seek time for every 3 seconds
     Organisation:      Peafowl inc.
*/
function updateSeekTimeAndPlay(assetId) {

  $.ajax({
        type: "POST",
        url: URL_LINK + "/getAssetData",
        data: '{"getAssetData":{"videoId":"' + assetId + '","userId":"' + storingIn + '","returnType":"tiny"}}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
          console.log('update seektime play ')
         storeSliderVisible = 0;
            var videoPlayTime = 0;
            var totalVideoDuration;
            var updateRecentlyWatched = 30;
                if (playVidInEpisodes == 0) {
                    
                    if (data.userData[0].subscription_status == "active"){
                        createRecentlyWatchedList(assetDataObj, assetId);
                         hideForVideoPlay(videoPlayerInEpisodes);

                    if (data.m3u8_url != null && data.m3u8_url != "") {
                       // videoUrlAsset = data.url_m3u8;
                       videoUrlAsset = data.m3u8_url;
                      //videoUrlAsset = "https://s3.amazonaws.com/peafowl/puma-store-dev/puma.mp4";
                    } else {
                       // videoUrlAsset = data.url;

                      videoUrlAsset = data.mp4_url;
                      
                      // videoUrlAsset = "https://s3.amazonaws.com/peafowl/puma-store-dev/puma.mp4";
                    }
                    var address = videoUrlAsset;
                     if(assetNextFlag == 1){
                        var videoContent = '<video  id="videoFile" class="backgroundvid" preload="auto"    ><source src="' + address + '" type="video/mp4"> Your browser does not support the video tag.</video>';

                    }
                    else{
                        var videoContent = '<video class="backgroundvid"  id="videoFile"  width="100%"  autoplay preload="auto"  ><source src="' + address + '" type="video/mp4"> Your browser does not support the video tag.</video>';
                    }
                    $("#video_player").append(videoContent);
                    videoFileId = document.getElementById("videoFile");
                    if(playNextFlag == 1){
                      
                      videoFileId.play();
                    }
                    myVar = setInterval(myTimer, 1000);
                    }
                    else{
                     window.location.href = 'expiry.html';
                    } 
                                         
                }
/*                $('#videoFile').on('progress', function (event) {
  $("#videoLoad").show();
});*/
            function myTimer() {
              
                updateRecentlyWatched++;
                temp_time = videoFileId.currentTime;
                totalVideoDuration = videoFileId.duration;
                var tempEndTime = totalVideoDuration-temp_time;
                if (updateRecentlyWatched >= 30) {
                    updateRecentlyWatched = 0;

                    //if (data.subscription_status == "active") {

                        $.ajax({
                            type: "POST",
                            url: URL_LINK + "/updateSeekTime",
                            data: JSON.stringify({
                                "updateSeekTime": {
                                    "userId": storingIn,
                                    "videoId": assetId,
                                    "seekTime": temp_time
                                }
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                                success: function(data) {
                               
                            },
                          error: function(jqXHR, textStatus, errorThrown) {
                                        
                          },
                          complete: function() {
                           /*  if(endVideoFlag == 1){
                             
                                endVideoFlag = 0;
                                clearInterval(myVar);
                            }*/
                          }
                        });
                   // }

                }

                if (temp_time > 0) {

                    if (videoPlayTime == temp_time) {
                    
                        if(flagValuePauseLoad == 0)
                        $("#videoLoad").show();
                    }else if(Math.round(tempEndTime) < 30 && flagNextPlay == 0){
                       
                       nextPlayCall = 1;
                    
                             getNextAssetData(assetId,nextAssetMyList);
                       
                    }
                     else if (Math.round(totalVideoDuration) == Math.round(temp_time)  ) {
                       
                        if(nextPlayCall == 0){
                          
                            getNextAssetData(assetId,nextAssetMyList);
                        }
                        else{
                        
                             $("#videoLoad").hide();
                       
                        //updateRecentlyWatched = 30;
                        //endVideoFlag = 1;
                        temp_time = 0;                        
                        $("#videoLoad").hide();
                         $("#video_player").html('');
                    $("#video_player").hide();
                    $("#videoLoad").hide();
                    $('#videoRedirect').show();
                    
                    $('#videoRedirect').html('<div class="VEwrapper animated2 fadeInRight"><div id="videFunc0"><div id="backVidRet" class="VEbackbtn">Back to Browse</div></div><div class="VEmovdescWrap"> <img src="'+assetNameImage+'" height="150" class="movtitle"/><h2 class="mov_alttxt">'+asetNextName+'</h2><div class="clearfix"></div><p class="VEmovfeat"><span>2H 2M</span><span class="sep">2016</span><span class="sep">13+</span></p><p class="VEMovieDesc">'+assetNextDesc+' </p><p class="VEmovfeat white">About to start in <span class="VEclock">'+varCountNext+' Sec</span></p><div id="videFunc1" class="videFuncActive"><div id="child0" class="VEplaybtn VEplaybtn_active">PLAY</div><div id="child1" class="VElistbtn" ></div></div><div class="clearfix"></div></div><!--VEmovdesc--><div class="VEgradient"></div><!--VEgradient--><div class="VEbgimage"></div></div>');
                    if(assetNextMyList == true){
                        $('#child1').html('-MY LIST');
                    }
                    else{
                        $('#child1').html('+MY LIST');
                    }
                    $('.VEbgimage').css("background-image","url("+assetMainImageNext+")");
                    
                     myVarNext = setInterval(myVideoNext, 1000);
                      clearInterval(myVar);
                        }
                       
                    } else {
                        videoPlayTime = temp_time;
                        $("#videoLoad").hide();
                    }

                }

            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // alert('error');

        },
        complete: function() {


        }
    });
}


function episodeVideoPlay(assetId, videoUrl) {
 
    var totalEpiVideoDuration;
 var updateRecentlyWatched = 30;
    var videoPlayTime = 0;
    $.ajax({
        type: "POST",
        url: URL_LINK + "/getAssetData",
        data: '{"getAssetData":{"videoId":"' + assetId + '","userId":"' + storingIn + '","returnType":"tiny"}}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
           if (data[0].subscription_status == "active") {
                  createRecentlyWatchedList(data, assetId);
                    hideForVideoPlay(videoPlayerInEpisodes);
                    if (data.m3u8_url != null && data.m3u8_url != "") {
                        videoUrlAsset = data.m3u8_url;
                       //videoUrlAsset = "https://s3.amazonaws.com/peafowl/puma-store-dev/puma.mp4";
                    } else {
                        videoUrlAsset = data.mp4_url;
                       //videoUrlAsset = "https://s3.amazonaws.com/peafowl/puma-store-dev/puma.mp4";

                    }

                     var address = videoUrlAsset;
                     if(assetNextFlag == 1){
                        var videoContent = '<video  id="videoFile" class="backgroundvid"     ><source src="' + address + '" type="video/mp4"> Your browser does not support the video tag.</video>';
                    }
                    else{
                        var videoContent = '<video class="backgroundvid"  id="videoFile"  width="100%"  autoplay    ><source src="' + address + '" type="video/mp4"> Your browser does not support the video tag.</video>';
                    }
                    $("#video_player").append(videoContent);
                    videoFileId = document.getElementById("videoFile");
                    if(playNextFlag == 1){
                      
                      videoFileId.play();
                    }
                    myVar = setInterval(myTimer, 1000);
                    }
                    else{
                     window.location.href = 'expiry.html';
                    } 
                                         
                
            function myTimer() {
              
                updateRecentlyWatched++;
                temp_time = videoFileId.currentTime;
                totalVideoDuration = videoFileId.duration;
                var tempEndTime = totalVideoDuration-temp_time;
                if (updateRecentlyWatched >= 30) {
                    updateRecentlyWatched = 0;

                    //if (data.subscription_status == "active") {

                        $.ajax({
                            type: "POST",
                            url: URL_LINK + "/updateSeekTime",
                            data: JSON.stringify({
                                "updateSeekTime": {
                                    "userId": storingIn,
                                    "videoId": assetId,
                                    "seekTime": temp_time
                                }
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                                success: function(data) {
                               
                            },
                          error: function(jqXHR, textStatus, errorThrown) {
                                        
                          },
                          complete: function() {
                           /*  if(endVideoFlag == 1){
                             
                                endVideoFlag = 0;
                                clearInterval(myVar);
                            }*/
                          }
                        });
                   // }

                }

                if (temp_time > 0) {

                    if (videoPlayTime == temp_time) {
                    
                        if(flagValuePauseLoad == 0)
                        $("#videoLoad").show();
                    }else if(Math.round(tempEndTime) < 30 && flagNextPlay == 0){
                       
                       nextPlayCall = 1;
                    
                             getNextAssetData(assetId,nextAssetMyList);
                       
                    }
                     else if (Math.round(totalVideoDuration) == Math.round(temp_time)  ) {
                       
                        if(nextPlayCall == 0){
                          
                            getNextAssetData(assetId,nextAssetMyList);
                        }
                        else{
                        
                             $("#videoLoad").hide();
                       
                        //updateRecentlyWatched = 30;
                        //endVideoFlag = 1;
                        temp_time = 0;                        
                        $("#videoLoad").hide();
                         $("#video_player").html('');
                    $("#video_player").hide();
                    $("#videoLoad").hide();
                    $('#videoRedirect').show();
                    
                    $('#videoRedirect').html('<div class="VEwrapper animated2 fadeInRight"><div id="videFunc0"><div id="backVidRet" class="VEbackbtn">Back to Browse</div></div><div class="VEmovdescWrap"> <img src="'+assetNameImage+'" height="150" class="movtitle"/><h2 class="mov_alttxt">'+asetNextName+'</h2><div class="clearfix"></div><p class="VEmovfeat"><span>2H 2M</span><span class="sep">2016</span><span class="sep">13+</span></p><p class="VEMovieDesc">'+assetNextDesc+' </p><p class="VEmovfeat white">About to start in <span class="VEclock">'+varCountNext+' Sec</span></p><div id="videFunc1" class="videFuncActive"><div id="child0" class="VEplaybtn VEplaybtn_active">PLAY</div><div id="child1" class="VElistbtn" ></div></div><div class="clearfix"></div></div><!--VEmovdesc--><div class="VEgradient"></div><!--VEgradient--><div class="VEbgimage"></div></div>');
                    if(assetNextMyList == true){
                        $('#child1').html('-MY LIST');
                    }
                    else{
                        $('#child1').html('+MY LIST');
                    }
                    $('.VEbgimage').css("background-image","url("+assetMainImageNext+")");
                    
                     myVarNext = setInterval(myVideoNext, 1000);
                      clearInterval(myVar);
                        }
                       
                    } else {
                        videoPlayTime = temp_time;
                        $("#videoLoad").hide();
                    }

                }

            }

                  /*  var address = videoUrlAsset;

                    var videoContent = '<video id="videoFile"  autoplay  ><source src="' + address + '" type="video/mp4"><source src="movie.ogg" type="video/ogg"> Your browser does not support the video tag.</video>';
                    $("#video_player").append(videoContent);
                    videoFileId = document.getElementById("videoFile");
                    myVar = setInterval(myTimer, 1000);*/
                
           

         /*   function myTimer() {
                temp_time = videoFileId.currentTime;
                totalEpiVideoDuration = videoFileId.duration;

                if (temp_time > 0) {

                    if (videoPlayTime == temp_time) {
                        $("#videoLoad").show();
                    } else if (totalEpiVideoDuration == temp_time) {
                        $("#videoLoad").hide();
                    } else {
                        videoPlayTime = temp_time;
                        $("#videoLoad").hide();
                    }

                }
            }
           } 
           else{
              window.location.href = 'expiry.html';
           }          */     


        },
        error: function(jqXHR, textStatus, errorThrown) {
            // alert('error');
        },
        complete: function() {


        }
    });

}

function videoPlayWithStore(assetId,resumePlayFlag){
 
  var subScribeStat;
  $.ajax({
        type: "POST",
        url: URL_LINK + "/getAssestData",
        data: '{"getAssestData":{"videoId":"' + assetId + '","userId":"' + storingIn + '","returnType":"tiny"}}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
          subScribeStat =  data[0].subscription_status;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // alert('error');

        },
        complete: function() {
       if(subScribeStat == "active"){
               hideForVideoPlay(videoPlayerInEpisodes);
   storeSliderVisible = 1;
    $.ajax({
    type: "POST",
    data: '{"gets3productDataFinal": {"bucket": "' + monetize_data_source_url + '"}}',
    url: URL_LINK + "/gets3productDataFinal",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (data) {
      
        videoLinks = data;
       // HRLURL = videoLinks.dataa[v].videoURL;
       //HRLURL = "https://s3.amazonaws.com/peafowl/puma-store-dev/puma.mp4";
        HRLURL = videoUrlAsset;
      //  HRLURL = "http://localhost:5000/1.mp4";
         //HRLPOSTER = videoLinks.dataa[v].posterURL;
         carleng = videoLinks.dataa.length;
         hms = timetosecondonly(videoLinks.dataa[v].duration);
         $("#videoLoad").hide();
        $("#HomeDiv").show(); 
        //$('#shopthecollection').css("visibility", "visible");
         //$('#showprod').css("visibility", "visible");
         var videoPlayTime = 0;
         var totalVideoDuration;
         var updateRecentlyWatched = 30;
         $("#video_playerStore").attr('src',HRLURL); 
         $("#video_playerStore").attr('poster',HRLPOSTER);   
         $("#player").css("z-index","0"); 
         createRecentlyWatchedList(assetDataObj, assetId);
        videoFileId = document.getElementById("video_playerStore");
        if(resumePlayFlag == 1){
         // myVar = setInterval(updateTimer, 1000);
         myCurrentTime = setInterval(myTimerCurrentValue, 1000);
        // myVar = setInterval(updateTimer, 1000);
        }
        else{
          
           myVar = setInterval(updateTimer, 1000);
        }

    function myTimerCurrentValue() {
       
         videoFileId.currentTime = watchedVideoFlag;
         myVar = setInterval(updateTimer, 1000);
        
      /*  if (videoFileId.currentTime == watchedVideoFlag) {
            alert('two');
              clearInterval(myCurrentTime);
              myVar = setInterval(updateTimer, 1000);
        }  
        else{
            alert('current time '+watchedVideoFlag);
            //temp_time = videoFileId.currentTime;
       
           videoFileId.currentTime = watchedVideoFlag;
        }    */      

    }
      // myVar = setInterval(updateTimer, 1000,assetId,updateRecentlyWatched,videoPlayTime,totalVideoDuration);
function updateTimer() {
        // alert("resumePlayFlag "+resumePlayFlag);
                updateRecentlyWatched++;
                temp_time = videoFileId.currentTime;
               var startAt = timetosecondonly(videoLinks.dataa[v].carousels[cl].startAt);
               var stopAt = timetosecondonly(videoLinks.dataa[v].carousels[cl].stopAt);
                totalVideoDuration = videoFileId.duration;
               /* if (temp_time > startAt && temp_time < stopAt){ 
                //alert("start stop");    
        $('#shopthecollection').html(monetize_label);
          $('#shopthecollection').css("visibility", "visible"); 
          $('#closhop').css("visibility", "visible"); 
    }*/
                var tempEndTime = totalVideoDuration-temp_time;
                if (updateRecentlyWatched >= 30) {
                    updateRecentlyWatched = 0;

                    //if (data.subscription_status == "active") {

                        $.ajax({
                            type: "POST",
                            url: URL_LINK + "/updateSeekTime",
                            data: JSON.stringify({
                                "updateSeekTime": {
                                    "userId": storingIn,
                                    "videoId": assetId,
                                    "seekTime": temp_time
                                }
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                                success: function(data) {
                               
                            },
                          error: function(jqXHR, textStatus, errorThrown) {
                              
                          },
                          complete: function() {
                             if(endVideoFlag == 1){
                                
                                endVideoFlag = 0;
                                clearInterval(myVar);
                            }
                          }
                        });
                   // }

                }

                if (temp_time > 0) {
                   // if(resumePlayFlag == 1){
                       
                    if (temp_time == watchedVideoFlag) {
                       
                         clearInterval(myCurrentTime);  
                         }
                   // }
                 
                  else if (videoPlayTime == temp_time) {
                        if(flagValuePauseLoad == 0){
                          if(showVideoLoad == 0){
                           
                            $("#videoLoad").show();
                          }
                        }
                        
                    }else if(Math.round(tempEndTime) < 20 && flagNextPlay == 0){  
                    
                       nextPlayCall = 0;
                        getNextAssetData(assetId,nextAssetMyList);
                    }
                  /*   else if (Math.round(totalVideoDuration) == Math.round(temp_time)) {
                      alert("totvid");
                        if(nextPlayCall == 0){
                          alert("totvid if");
                            getNextAssetData(assetId,nextAssetMyList);
                        }
                        else{
                          alert("else nxt");
                             $("#videoLoad").hide();
                       
                        //updateRecentlyWatched = 30;
                        endVideoFlag = 1;
                        temp_time = 0;                        
                        $("#videoLoad").hide();
                        $("#HomeDiv").hide();
                        $('#showprod').css("visibility", "hidden"); $("#video_playerStore").attr('src',"");                 
                        $("#videoLoad").hide();
                         $("#video_player").html('');
                    $("#video_player").hide();
                    $("#videoLoad").hide();
                    $('#videoRedirect').show();
                    
                    $('#videoRedirect').html('<div class="VEwrapper animated2 fadeInRight"><div id="videFunc0"><div id="backVidRet" class="VEbackbtn">Back to Browse</div></div><div class="VEmovdescWrap"> <img src="'+assetNameImage+'" height="150" alt="Intelliget Boss" class="movtitle"/><h2 class="mov_alttxt">'+asetNextName+'</h2><ul class="VEstarrating"><li><img src="images/star.svg"/></li><li><img src="images/star.svg"/></li><li><img src="images/star.svg"/></li><li><img src="images/star.svg"/></li><li><img src="images/star.svg"/></li></ul><div class="clearfix"></div><p class="VEmovfeat"><span>2H 2M</span><span class="sep">2016</span><span class="sep">13+</span></p><p class="VEMovieDesc">'+assetNextDesc+' </p><p class="VEmovfeat white">About to start in <span class="VEclock">'+varCountNext+' Sec</span></p><div id="videFunc1" class="videFuncActive"><div id="child0" class="VEplaybtn VEplaybtn_active">PLAY</div><div id="child1" class="VElistbtn" ></div></div><div class="clearfix"></div></div><!--VEmovdesc--><div class="VEgradient"></div><!--VEgradient--><div class="VEbgimage"></div></div>');
                    if(assetNextMyList == true){
                        $('#child1').html('-MY LIST');
                    }
                    else{
                        $('#child1').html('+MY LIST');
                    }
                    $('.VEbgimage').css("background-image","url("+assetMainImageNext+")");
                    
                     myVarNext = setInterval(myVideoNext, 1000);
                      clearInterval(myVar);
                  }
                       
                    }*/
                     else {
                       // alert('video load');
                        videoPlayTime = temp_time;
                        $("#videoLoad").hide();
                    }

                }

            }

         $("#slider").addClass("slider"+v+carousel_value);
         $("#sliderfor").addClass("slider-for"+v+carousel_value);

         $(".slider"+v+carousel_value).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            centerMode: true,
            asNavFor: ".slider-for"+v+carousel_value,
            infinite: true,
           // variableWidth:false
        });

        $(".slider-for"+v+carousel_value).slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: ".slider"+v+carousel_value,
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: true,
           // variableWidth:false
        }); 
         getproducts();
         fetchaddress(); 
        
   
    },
    error: function (jqXHR, textStatus, errorThrown) {
        // alert("  ");
      
    }
});
          }
          else{
            window.location.href = 'expiry.html';
          }
        }
    });
}

function fetchaddress(){

  // alert('sdfdf storingIn' +storingIn);
//storingIn =  "58e36ddf1f23feff3631d37c";


var fetch_address  = { "fetchAddressStore": { "user_id": storingIn }}
$.ajax({
        url:  URL_LINK + "/fetchAddressStore",
        type: "POST",
        crossDomain: true,
        data: JSON.stringify(fetch_address),
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
           addresses = data;
          
            if (data.length != 0)   // address is present.
            {
                
                $("#address_Count").html("("+data[0].address.length+")");
                for (var k = 0; k < data[0].address.length; ++k) {
                        if(data[0].address[k].defaultAddress==true){
                           var classaddressact = 'adr_wrp_act';
                        }else {
                          var classaddressact= '';
                        }

                    $("#addressbook").append('<div id="adr_wrp'+k+'" class="adr_wrp '+classaddressact+'"><div class="col-md-6"><h3>Billing</h3><p>' + data[0].address[k].billingAddress.streetAddress + " " + data[0].address[k].billingAddress.extendedAddress + "," + data[0].address[k].billingAddress.locality + "," + data[0].address[k].billingAddress.region + "," + data[0].address[k].billingAddress.postalCode + '</p></div><div class="col-md-6"><h3>Shipping</h3><p>' + data[0].address[k].shippingAddress.streetAddress + " " + data[0].address[k].shippingAddress.extendedAddress + "," + data[0].address[k].shippingAddress.locality + "," + data[0].address[k].shippingAddress.region + "," + data[0].address[k].shippingAddress.postalCode + '</p></div><div class="clearfix"></div></div>');                  
                }

             address_defaultindex = addresses[0].address.findIndex(function(element) {
                  return element.defaultAddress == true;
                });

            }else {
                    $("#addressbooknew").empty().append('<br/><strong>Please provide Billing and Shipping details.</strong><br/><strong>Go to '+mainUrl+'</strong><br/><strong>My Account</strong><br/><strong>Add Billing details</strong><div class="adr_cn"><div class="adrr_btn btnact" id="refbut" >RESUME</div></div>');

                }
            },
        error: function (jqXHR, textStatus, errorThrown) {

        }
});
}

function payWithCVV(bill) {
 
    var storage = window.localStorage;
    var ship_address = bill.shippingAddress;
    var bill_address = bill.billingAddress;
    var userNameValue = storage.getItem("fullNameValue");
    var userIdValue = storage.getItem("userIdVal");
    var customerIdValue = storage.getItem("customer_idValue");
    var userEmail = storage.getItem("emailValue");
    
    $.ajax({
        type: "POST",
        url:  URL_LINK + "/payWithCVV",

        data: JSON.stringify({ "payWithCVV": { "email": userEmail, "billingAddress": bill_address, "shippingAddress": ship_address, "userName": userNameValue, "productName": selectedProductName, "productPrice": selectedProductPrice, "productColor": selectedProductColor, "productSize": selectedProductSize, "productQuantity": selectedProductQty, "productDeliveryCharge": "Free", "userPhoneNumber": "", "productImage": selectedProductImage, "productId": selectedProductId, "userId": userIdValue, "customerId": customerIdValue,"clearancePrice": selectedUnitPrice } }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          
           var OS_orderDate =datFormat(data.order.orderDate);
          

          $('#OS_OrderId').html(data.order.orderId);
          $('#OS_OrderDate').html(OS_orderDate);
          $('#OS_OrderProduct').html(data.order.productName);
          $("#OS_OrderImg").attr('src', data.order.productImage);
          $('#OS_OrderPrice').html(data.order.productPrice);
        //  $('#OS_OrderColor').html(data.order.productColor);

          $("#OS_OrderColor span").css("background-color",data.order.productColor);

          $('#OS_OrderSize').html(data.order.productSize);
          $('#OS_OrderQty').html(data.order.productQuantity);
          $('#OS_OrderBilling').html(data.order.deliveryAddressLine1+''+data.order.deliveryAddressLine2+''+data.order.deliveryAddressLine3);

          $("#orderblock").show();

          
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(" --  --");
        }
    });
}

function payWithCVVDonation(price,bill) {
 //alert("test pay "+price);
 //alert("bill "+JSON.stringify(bill));
    var storage = window.localStorage;
    var ship_address = bill.shippingAddress;
    var bill_address = bill.billingAddress;
    var userNameValue = storage.getItem("fullNameValue");
    var userIdValue = storage.getItem("userIdVal");
    var customerIdValue = storage.getItem("customer_idValue");
    var userEmail = storage.getItem("emailValue");
    
    $.ajax({
        type: "POST",
        url:  URL_LINK + "/payWithCVVDonation",

        data: JSON.stringify({"payWithCVVDonation":{"amount":price,"transaction_id":"","date":"","email":userEmail,"billingAddress":bill_address,"shippingAddress":ship_address,"userName":userNameValue,"productName":"","productPrice":"","productColor":"","productSize":"","productQuantity":"","productDeliveryCharge":"","userPhoneNumber":"","productImage":"","cvv":"","userId":userIdValue,"customerId":customerIdValue}}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
          
          //alert(JSON.stringify(data));
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(" --  --");
        }
    });
}

function datFormat(dateObject) {
        var d = new Date(dateObject);

        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        var months = new Array(12);
        months[1] = "Jan";
        months[2] = "Feb";
        months[3] = "Mar";
        months[4] = "Apr";
        months[5] = "May";
        months[6] = "Jun";
        months[7] = "Jul";
        months[8] = "Aug";
        months[9] = "Sep";
        months[10] = "Oct";
        months[11] = "Nov";
        months[12] = "Dec";


        if (day < 10) {
            day = "0" + day;
        }
        
        var date = day + " " + months[month] + " " + year;

    return date;
}


function timetosecondonly(time){
    var hmis = time;  
    var a = hmis.split(':'); 
    var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
    return seconds;
}

function getproducts(){
    var productsData = videoLinks.dataa[v].carousels[carousel_value].products;
    var prolength =videoLinks.dataa[v].carousels[carousel_value].products.length;
    var cartlogo = videoLinks.dataa[v].carousels[carousel_value].carouselLogo;
curint = setInterval(function(){ 
    if(videoLinks!=undefined){
        showhide();
    }
}, 1000);
$("#videoLoad").hide();
//$('#showprod').css("visibility", "visible");
$(".slider"+k+kl).slick('unslick').empty(); 

$("#slider").removeClass("slider"+k+kl);

$(".slider-for"+k+kl).slick('unslick').empty(); 
$("#sliderfor").removeClass("slider-for"+k+kl);

$("#slider").addClass("slider"+v+carousel_value);
$("#sliderfor").addClass("slider-for"+v+carousel_value);
  




    for (var i = 0; i < prolength; ++i) {
    $(".slider"+v+carousel_value).append('<div id="storeCar'+ i +'" class="slick-slideStore" title="'+ productsData[i].id +'" ><img id="storageCar'+ i +'" title="'+ productsData[i].price +'"  width="100%" class="liImage" alt="'+ productsData[i].id +'" src="' + ul_ext + productsData[i].images[0].thumb + '"  id="img' + parseInt(i + 1) +'"   ></div>');
     if(monetize_type == "User Defined Donation" || monetize_type == "Fixed Donation"){
       $(".slider-for"+v+carousel_value).append('<div class="slick-slideStore"><div class="logo" id="logo1" ><img  src="'+cartlogo+'" /></div><ul id="priceId"><li id="sbprice">DONATE: <span class="pricered">$' + productsData[i].price + '</span></li></ul><div id="btnSubmit" class="button pointer clickbuy btMargin" alt="'+ productsData[i].id +'">DONATE</div></div>');
       $('#btnSubmit').html('DONATE');
       
    }
    else{
      $(".slider-for"+v+carousel_value).append('<div class="slick-slideStore"><div class="logo" id="logo1" ><img  src="'+cartlogo+'" /></div><ul id="priceId"><li id="sbid">"' + productsData[i].id + '"</li><li id="sbdesc">' + productsData[i].title + '</li><li id="sbprice">Our Price: <span class="pricered">$' + productsData[i].price + '</span></li></ul><div id="btnSubmit" class="button pointer clickbuy" alt="'+ productsData[i].id +'">BUY NOW</div></div>');
    }
    
       
        $('#prod_Count').html(prolength);
   
        if (parseInt(i + 1) == prolength) {
            
            $(".slider-for"+v+carousel_value).slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              centerMode: true,
              asNavFor: ".slider"+v+carousel_value,
              infinite: true,
              variableWidth: false  
            });

            $(".slider"+v+carousel_value).slick({
              slidesToShow: 6,
              slidesToScroll: 1,
              asNavFor: ".slider-for"+v+carousel_value,
              dots: false,
              centerMode: false,
              focusOnSelect: false,
              infinite: true,
              variableWidth: false
            });

            $(".slider"+v+carousel_value).addClass("homeactive");

            k = v;
            kl = carousel_value;
        }

         storeValuePrice = ($("#storeCar"+0).attr("title"));
    }
}

function showhide() {
  
    if(getcur>hms){
        bvlink();
    }
setslider++;
//console.log('setslider '+setslider);
if(setslidershow < setslider ){
    slidershow = false;
}
player = document.getElementById("video_playerStore");
getcur = player.currentTime;


if (parseInt(getcur) > 1) {
 $("#loaderDiv").hide();
}

    var caroleng = videoLinks.dataa[v].carousels.length;

        if(cl<=(caroleng-1))
        {
            cl = cl;
        }else{
             cl = 0;
        }

    var startAt = timetosecondonly(videoLinks.dataa[v].carousels[cl].startAt);
    var stopAt = timetosecondonly(videoLinks.dataa[v].carousels[cl].stopAt);

    if (getcur > startAt && getcur < stopAt){
     
      $('#shopthecollection').html(monetize_label);
        $('#shopthecollection').css("visibility", "visible"); 
        $('#closhop').css("visibility", "visible"); 
        clearInterval(curint);
    }else {
      
       if(slidershow==false){ 
        $('#shopthecollection').css("visibility", "hidden");
        $('#closhop').css("visibility", "hidden");
        $("#shopthecollection").removeClass("hideshop");
        $("#closhop").removeClass("hideshop");
        //$('#showprod').css("visibility", "hidden");
         $('#cloprod').css("display", "none");
        }
    }
}

function bvlink() {
    if(v==carleng-1){
        v = 0;
        cl = 0;
        carousel_value=0;
        hms = timetosecondonly(videoLinks.dataa[v].duration);
        HRLURL = videoLinks.dataa[v].videoURL;
        HRLPOSTER = videoLinks.dataa[v].posterURL;
    }else {
        v = v+1;
        cl = 0;
        carousel_value=0;
        hms = timetosecondonly(videoLinks.dataa[v].duration);
        HRLURL = videoLinks.dataa[v].videoURL;
        HRLPOSTER = videoLinks.dataa[v].posterURL;
    }
    videobl(HRLURL,HRLPOSTER);
}

function videobl(HRLURL,HRLPOSTER) {
    $("#video_playerStore").attr('src',HRLURL); 
    $("#video_playerStore").attr('poster',HRLPOSTER);  
}

function getNextAssetData(nextAssetId,myListStat){
  
     var storage = window.localStorage;
    var totalEpiVideoDuration;
    playNextFlag = 0;
    flagNextPlay =1;
    //var videoPlayTime = 0;
    $.ajax({
        type: "POST",
        url: URL_LINK + "/getNextPlay",
        data: '{"getNextPlay":{"videoId":"' + nextAssetId + '","userId":"' + storingIn + '","myList":"'+myListStat+'","deviceId":"'+storage.getItem(keyDeviceId)+'"}}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
       
            assetNextId = data.assetId;
           assetNameImage = data.title_img_url_700x400;
           asetNextName = data.assetName;
           assetNextDesc = data.description;
           assetMainImageNext = data.banner_image_1300x650;
           assetNextMyList = data.userData[0].myList;
           monetize = data.monetize;
            monetize_type = data.monetizeType;
            monetize_label = data.monetizeLabel;
            monetize_data_source_url = data.monetize_data_source_url;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // alert('error');
        },
        complete: function() {
          
            if(nextPlayCall == 0){
                 $("#videoLoad").hide();
                       storeSliderVisible = 0;
                        //updateRecentlyWatched = 30;
                        endVideoFlag = 1;
                        temp_time = 0;
                        $("#HomeDiv").hide();
                        $('#showprod').css("visibility", "hidden"); $("#video_playerStore").attr('src',"");                 
                        $("#videoLoad").hide();
                         $("#video_player").html('');
                    $("#video_player").hide();
                    $("#videoLoad").hide();
                    $('#videoRedirect').show();
                    
                    $('#videoRedirect').html('<div class="VEwrapper animated2 fadeInRight"><div id="videFunc0"><div id="backVidRet" class="VEbackbtn">Back to Browse</div></div><div class="VEmovdescWrap"> <img src="'+assetNameImage+'" height="150"  class="movtitle"/><h2 class="mov_alttxt">'+asetNextName+'</h2><div class="clearfix"></div><p class="VEmovfeat"><span>2H 2M</span><span class="sep">2016</span><span class="sep">13+</span></p><p class="VEMovieDesc">'+assetNextDesc+' </p><p class="VEmovfeat white">About to start in <span class="VEclock">'+varCountNext+' Sec</span></p><div id="videFunc1" class="videFuncActive"><div id="child0" class="VEplaybtn VEplaybtn_active">PLAY</div><div id="child1" class="VElistbtn" ></div></div><div class="clearfix"></div></div><!--VEmovdesc--><div class="VEgradient"></div><!--VEgradient--><div class="VEbgimage"></div></div>');
                    if(assetNextMyList == true){
                        $('#child1').html('-MY LIST');
                    }
                    else{
                        $('#child1').html('+MY LIST');
                    }
                    $('.VEbgimage').css("background-image","url("+assetMainImageNext+")");
                    
                     myVarNext = setInterval(myVideoNext, 1000);
                      clearInterval(myVar);
            }
        }
    });
}

function myVideoNext(){
    varCountNext--;
    playNextFlag = 0;
    
   $('.VEclock').html(varCountNext+" sec");
   if(varCountNext == 0){
    // divsNxtFunc = $('#videFunc').find('div').length;
    varCountNext = 5;
    flagNextPlay =0;
    $('#videoRedirect').hide();

        $("#video_player").show();
        $("#videoLoad").show();
    videoFileId.play();
    assetNextFlag = 0; 
    nextPlayCall = 0;

    clearInterval(myVarNext);
   }
   if(varCountNext == 3){
  
    assetNextFlag = 1;
      //displayOfInnerDescribedPage(storingIn,assetNextId,1);
     // updateSeekTimeAndPlay(assetNextId)
      endVideoFlag = 0;
     displayOfInnerDescribedPage(storingIn,assetNextId);
     /* if(monetize == "true" || monetize != ""){
        
      videoPlayWithStore(assetNextId,0);
      }else{
      updateSeekTimeAndPlay(assetNextId);
      } */
    // resumeButtonVisible(assetNextId);
   }
}




function resumeButtonVisible(assetId) {
    var videoResumeUrl;
    videoStartTime = 1;
   
    $.ajax({
        type: "POST",
        url: URL_LINK + "/getAssetData",
        data: '{"getAssetData":{"videoId":"' + assetId + '","userId":"' + storingIn + '","returnType":"tiny"}}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
           
         //if (data.subscription_status == "active")
            createRecentlyWatchedList(assetDataObj, assetId);
            //if (data.watchedVideo > 0) {
                if (data.m3u8_url != null && data.m3u8_url != "") {
                    videoResumeUrl = data.m3u8_url;
                } else {
                    videoResumeUrl = data.mp4_url;

                }
                playVideoResume(videoResumeUrl, data.assetId, data.userData[0].watchedVideo, data.userData[0].subscription_status);
           // }         
        },
        error: function(jqXHR, textStatus, errorThrown) {},
        complete: function() {


        }
    });
}

function playVideoResume(videoUrl, video_id, fetchTime, subscribeStatus) {
    var totalResumeVideoDuration;
    var videoPlayTime = 0;
    var updateRecentlyWatched = 30;
    flagResumePlay = 1;
    hideForVideoPlay(videoPlayerInEpisodes);
    var address = videoUrl;
   
       if(assetNextFlag == 1){
                        var videoContent = '<video  id="videoFile"  width="100%"  class="backgroundvid"   ><source src="' + address + '" type="video/mp4"> Your browser does not support the video tag.</video>';
                    }
                    else{
                        var videoContent = '<video  id="videoFile"  width="100%"  autoplay class="backgroundvid"   ><source src="' + address + '" type="video/mp4"> Your browser does not support the video tag.</video>';
                    }
    /*var videoContent = '<video align="right" id="videoFile" width="100%" autoplay  ><source src="' + address + '" type="video/mp4"><source src="movie.ogg" type="video/ogg"> Your browser does not support the video tag.</video>';*/

    $("#video_player").append(videoContent);
    videoFileId = document.getElementById("videoFile");
    myVar = setInterval(myTimer, 1000);
   // if (subscribeStatus == "active")
        myCurrentTime = setInterval(myTimerCurrentValue, 1000);

    function myTimerCurrentValue() {

        videoFileId.currentTime = fetchTime;

    }

    function myTimer() {

        updateRecentlyWatched++;

        temp_time = videoFileId.currentTime;
        totalResumeVideoDuration = videoFileId.duration;
        var tempEndTime = totalResumeVideoDuration-temp_time;
        if (updateRecentlyWatched >= 30) {
            updateRecentlyWatched = 0;
           // if (subscribeStatus == "active")
                myPlayerTimerUpdate();
        }
        if (subscribeStatus == "active" || subscribeStatus == "expired") {
            if (temp_time == fetchTime) {
                 clearInterval(myCurrentTime);  
                 }            
              else if (videoPlayTime == temp_time) {
                 if(flagValuePauseLoad == 0)
                    $("#videoLoad").show();
                }else if(Math.round(tempEndTime) < 30 && flagNextPlay == 0){
                       // alert('vide')
                        getNextAssetData(video_id,nextAssetMyList);
                    }
                 else if (totalResumeVideoDuration == temp_time) {
                    $("#videoLoad").hide();
                        endVideoFlag = 1;
                        temp_time = 0;                        
                        $("#videoLoad").hide();
                         $("#video_player").html('');
                    $("#video_player").hide();
                    $("#videoLoad").hide();
                    $('#videoRedirect').show();
                    
                    $('#videoRedirect').html('<div class="VEwrapper animated2 fadeInRight"><div id="videFunc0"><div id="backVidRet" class="VEbackbtn">Back to Browse</div></div><div class="VEmovdescWrap"> <img src="'+assetNameImage+'" height="150"  class="movtitle"/><h2 class="mov_alttxt">'+asetNextName+'</h2><ul class="VEstarrating"><li><img src="images/star.svg"/></li><li><img src="images/star.svg"/></li><li><img src="images/star.svg"/></li><li><img src="images/star.svg"/></li><li><img src="images/star.svg"/></li></ul><div class="clearfix"></div><p class="VEmovfeat"><span>2H 2M</span><span class="sep">2016</span><span class="sep">13+</span></p><p class="VEMovieDesc">'+assetNextDesc+' </p><p class="VEmovfeat white">About to start in <span class="VEclock">'+varCountNext+' Sec</span></p><div id="videFunc1" class="videFuncActive"><div id="child0" class="VEplaybtn VEplaybtn_active">PLAY</div><div id="child1" class="VElistbtn" ></div><div id="child2" class="VElistbtn">TRAILER</div></div><div class="clearfix"></div></div><!--VEmovdesc--><div class="VEgradient"></div><!--VEgradient--><div class="VEbgimage"></div></div>');
                    if(assetNextMyList == true){
                        $('#child1').html('-MY LIST');
                    }
                    else{
                        $('#child1').html('+MY LIST');
                    }
                    $('.VEbgimage').css("background-image","url("+assetMainImageNext+")");
                    
                     myVarNext = setInterval(myVideoNext, 1000);
                      clearInterval(myVar);

                } else {
                    videoPlayTime = temp_time;
                    $("#videoLoad").hide();
                }
            
        } else {
            if (temp_time > 0)
                $("#videoLoad").hide();
        }

    }

    function myPlayerTimerUpdate() {

        updatedTime = videoFileId.currentTime;

        $.ajax({
            type: "POST",
            url: URL_LINK + "/updateSeekTime",
            data: JSON.stringify({
                "updateSeekTime": {
                    "userId": storingIn,
                    "videoId": video_id,
                    "seekTime": updatedTime
                }
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {

            },
            failure: function(errMsg) {}
        });

    }
}

/*Function for network connectivity checking*/
function checkNetworkConnectivity(){

     var networkState = navigator.connection.type;
         if (networkState == Connection.NONE){
            
              return 0;
        }
        else{
            //var popup = document.getElementById('myPopup');
            //popup.classList.toggle('show');
            return 1;
        }    
}

function onDatabaseInsertion(source,error,code,details,device){
   
            var db = null;
            db = sqlitePlugin.openDatabase({name: 'damedashStudios.db',location: 'default'});
            db.transaction(function(transaction) {
               
                transaction.executeSql('INSERT INTO damedashStudios_error_log (source, error,code,details,device) VALUES (?,?,?,?,?)', [source,error,code,details,device],
                function(tx, result) {
                },
                function(error) {
                });
                });
}

function getGoal(id) {
   
    for(var d=0;d<videoLinks.dataa.length;d++){
        
        var clength = videoLinks.dataa[d].carousels.length;
        for(var cli=0;cli<clength;cli++){
           
            var caro = videoLinks.dataa[d].carousels[cli].products; 
            
            for(var c=0;c<caro.length;c++){
              
                if(caro[c].id == id)
                return _.find(caro, function(goal) {
                    return goal.id == id;
                });
            }
        }
    }
}

function PlayPause() {
    if (player.paused == false) {
        player.pause();
    } else {     
        player.play();
    }
}

function productdetails(product){
   console.log("products "+JSON.stringify(product));
    var previewImg = ul_ext+product.images[0].large;
    var product_imglength = product.images.length;
    var product_colorlength = product.colors.length;
    var product_sizelength = product.sizes.length;
   
    $('#preview_Img').attr("src",previewImg);
    $('#preview_Desc').html(product.description);
    $('#preview_Title').html(product.title);
    $('#preview_Sku').html("SKU - "+product.sku);
    $('#preview_Price').html('$'+product.clearancePrice);
    

     for (var i = 0; i < product_imglength; ++i) {
        if(i==0){
           var classact = 'act';
        }else {
          var classact= '';
        }
        $("#preview_Thumb").append('<li id="thumb'+i+'"  class="'+classact+' borderStore"><img src="'+ul_ext+product.images[i].thumb+'"></li>');
     }
   
     for (var c = 0; c < product_colorlength; ++c) {
        if(c==0){
           var classcoloract = 'act';
        }else {
          var classcoloract= '';
        }
        $("#preview_Color").append('<span id="color'+c+'" class="'+classcoloract+' colorborder" style="background-color:'+product.colors[c]+'"></span>');
     }
   

    for (var s = 0; s < product_sizelength; ++s) {
        $("#sizeDimen").append('<span id="sizeid'+s+'" class="sizeborder" >'+product.sizes[s]+'</span>');
     }
     

    $("#ProductDetails").show();
   // $("#HomeDiv").hide();
   $("#HomeDiv").css("visibility", "hidden");
   $('.snb').css("display", "none");
    $('#showprod').css("visibility", "hidden");
    
}



