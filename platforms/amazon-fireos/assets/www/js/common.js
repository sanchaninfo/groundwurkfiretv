/**
 *
  Module Name :      
  File Name   :      common.js
 Project     :      damedashstudios
  Copyright (c) Damedash Studios.
  author      :      Siddartha.
  author      :      
  license     :   
  version     :      1.0.0 // written by siddartha for present. 
  Created on  :      6 august 12:20 pm
  Last modified on:  30th November 
  Description :      This file contains url links and global variables which are used through out the application.  
  Organisation:      Peafowl inc.
 */

/*var APP_ENV  = "PROD";

var URL_LINK = "http://dev.damedashstudios.com";

if(APP_ENV == "PROD")
  URL_LINK = "http://www.damedashstudios.com";*/


//URL_LINK = "http://alleyezonme.groundwurk.com";


var URL_LINK = "http://34.207.34.102";

//modified by J Sachin Singh on 18 August 2016 for declaring variables required for registration.js
/*variables for storing device details and user details locally*/
var keyModel="modelValue";
 var keyManufacturer="manufacturerValue";
 var keyDeviceName="device_nameValue";
 var keyDeviceId="device_idValue";
 var keyDeviceMacAddress="device_mac_addressValue";
 var keyBrandName="brand_nameValue";
 var keyHostName="host_nameValue";
 var keyDisplayName="display_nameValue";
 var keySerialNumber="serial_numberValue";
 var keyUUIDValue="uuid_numberValue";
 var codeValue="code_numberValueDevice";
 var _idValue="id_numberValue";
 var pairStatus="pairStatusValue";
 var damedash_uuidExist = "damedash_uuidExist";
 var resumeFlag = "resumeFlag";
var STOREVISIBLE = false;
 var userId="userIdVal";
 var user="userValue";
 var password="passwordValue";
 var email="emailValue";
 var fullName="fullNameValue";
 var created="createdValue";
 var last_login="last_loginValue";
 var logoutTime="logoutTimeValue";
 var subscription_type="subscription_typeValue";
 var subscription_start="subscription_startValue";
 var subscription_end="subscription_endValue";
 var subscription_id="subscription_idValue";
 var subscription_plan_id="id_numberValue";
 var subscription_status="subscription_statusValue";
 var user_status="user_statusValue";
 var user_from="user_fromValue";
 var user_id="user_idValue";
 var customer_id="customer_idValue";
 var imageUserId="imageValue";
 var damedash_id="damedash_idValue";

 /*end of storing device details and user details locally*/

 /* variables in landing page */

 var episode_num=1;
 var season_num = 0;  
 var flagRecentTest=0;
 var flagEpisodeVal=0;
 var flagEpisodeVisible=0;
 var slideBackClickCount=0;
 var seekTimeValue;
 var flagValuePause=0;
 var StoreValue;
 var flagRecent=0;
 var flagSeekTimeValue=0;
 var timer;
 var timerCategory;
 var bgimage;
 var bgimageCat;
 var catMenuVal = 0;
var storingIn;
 var storingIn = "59dc9db1f698d1a5593d8233";
 //var storingIn = "58f48e23a1f59c490c100bb3"  
 
var deviceId = "b5370979cc8328d5";
var slideImage=[];
var slideImageName = [];
 var sliderTimer;
  //var storingIn = "57ff68ab504352550ad75d23" 
  //var deviceId = "a5c1c9de6e692103";
 //var storingIn = "58e0c678659916ad3b199638" 
//var deviceId = "B7DB7EDF-5C21-4319-BE12-BE07DFCD2B61";
 var flagVideoDiv=0;
 var myListIdValueInStr;
 var flagListValue=0;
 var videoFileId;
 var myListIdValue;
 var flagListBackValue=0;         
 var flagRecent=0;
 var flagAdd=0;
 var flagAddRem=0;
 var isDescriptionDivVisible;
 var isThumbsListVisible;
 var episodeClick=1;
 var flagVariable=0;
 var flagEnterVariable=0;
 var flagDivVal=0;
 var flagRemVal=0;
 var flagPresentInList=0;
 var flagAssetStatus=0;
 var descriptionDisplayValue;
 var countDownClickVar=0;
 var countDownClickVarLanding=0;
 var carouselCountId=0;
 var addRemoveListCount=0;
 var watchedVideoFlag=0;
 var myListPresentFlag;
 var flagListAddRemove=0;
 var descriptionDivFlag=0; 
 var isVisible;
 var isVisibleMenuDiv;
 var isVisibleMovieDiv;
 var myListDataLen = 0;
 var temp_time = 0;
 var updatedTime = 0;
 var myVar;
 var myVarTimeUpdate;
 //var countEpisodeVideoClick = 1;
 var seconds;
 var minutes;
 var hours;
 /*  End of variables in categories page.*/
var NavItems = [];
var imgSlideData = [];
var CarouselItems = [];
var cnt = 0;
var mylistname = 'My List';
var recentlywatchedname = 'Recently Watched';
var recentWatchAvailable = 0;
var myListAvailable = 0;
var recentlength=0;
var mylistlength=0;
var storelength = 0;
var hideclass;
var categorycount=0;
var categoryValueCount=0;
var rightMov=0;
var topMov=0;
var topMovSearch = 150;
var episodetopMov =0;
var categoryDownClick=0;
var getCategoryValue;
var catLstId;
var searchLstId;
var descriptionFlag=0;
var marginValue=356;
 var marginValueSearch=417;
 var rightMovSearch = 0;
var flagElse=0;
var downMov=0;
var upClickCat=0;
var downCount=1;
var flagMainDivVisible=0;
var flagAddList=0;
var countForCarousel=0;
var flagAddListBack=0;
var pageVisible=0;
var landingCarouselFlag=0;
var episodeCarouselFlag=0;
var videoPlayerInEpisodes=0;
var flagRightClickCat=0;
var resumeEpisodeStat=0;
var divVisibleLanding=0;
var rightAssetFound=0;
var flagButtonMovement=0;
var flagResumePlay = 0;
var topmovdownclick = 204;
var topMovSearchDim = 241;
var topmovdownclickEpisode = 220;
var register;
var userSubscription;
var createData;
var registeredUser;
var flagSeason = 0;
var seasons = [];
var categoryVisibleChange = true;
var flagSeasonPresent = 0;
var episodeDiv = 0;
var episodeAssetDownClick = 1;
var playVidInEpisodes = 0;
var catSelectCount = 1;
var localBackButtonFlag="localBack";
var landingBack ="landBack";
var loadFlag = 0;
 var backLanding = 0;
 var assetNameValue;
 var assetDescriptionValue;
 var assetDurationValue;
 var assetYearValue;
 var assetImageValue;
 var tvShowStatus;
 var assetDataObj;
 var videoUrlAsset;
 var containsObj;
 var seasonNumValue;
 var episodeNumValue;
 var videoStartTime = 0;
 var myCurrentTime;
 var carouselArray =[];
 var keyPadCount = 1;
var character = "";
var countDownKeyPad = 0;
var keyPadValue = 2;
var searchCount = 0;
var searchCountAsset = 0;
var searchRightClick = 0;
var leftClickSearchFun = 3;
 var searchCountValue = 0;
 var flagSearchDescribe = 0;
 var assetDataValues;
 var settingsClick = 0;
 var flagValuePauseLoad = 0;
 var expiredStatus = 0;
 var registrationFlagTrue = 0;
 var launchCheckUUIDExistFlag = 0;
var slideShowVal = 0;
var slickVal = 0;
var changeSlick = 0;
var flagAddListRecent = 0;
var flagAppPause = 0;
var catListCount = 0;
var catLastUpdatedVal = 0;
var endVideoFlag = 0;
var nextAssetMyList;
var assetNameImage;
var asetNextName;
var assetNextDesc;
var assetNextId;

var assetMainImageNext;
var myVarNext;
var varCountNext = 5;
var assetNextFlag = 0;
var nxtVidFunc = 0;
var divsNxtFunc;
var videoRedirectActive =1;
var assetNextMyList;
var nextPlayCall = 0;
var carouselValue = 0;
var photoCarouselLen=0;
var slideShower = 0;
var mainDivFlag = 0;

/*test variables*/

var videoLinks;
var HRLURL;
var HRLPOSTER;
var carousel_value = 0;
var mainUrl = "http://peafowl-dev.alleyezonmethemovie.com/";
var videoDataUrl = "gets3productData";
var videoLinks;
var v = 0;
var cl = 0;
var hms;
var k = v;
var kl = carousel_value;
var setslider = 0;
var setslidershow = 5;
var slidershow = false;
var player='';
var getcur='';
var ul_ext = "https://s3.amazonaws.com/peafowlbbfdev/bbfdev";
var storeSliderVisible = 0;
var filteredProduct;

var homepage = true;
var productspage = false;
var addresspage = false;
var ordersuccesspage = false;

// Home page Opening Variables Should be like this
var homepageright = true;
var homepageleft = false;
var homepagebottom = true;

// Product page Opening Variables Should be like this
var productpageleft = true;
var productpageright = false;

var productDisplayFlag = true;
var productDimenSelectionFlag = true;

var countSize = 0;
var countDownClickProduct=0;
var colorClick = 0;
var quantityCount = 1;
 var curint;
var monetize ;
var monetize_type;
var monetize_label;
var monetize_data_source_url;
var className;
var fetchAddrressUrl = "fetchAddressStore";
var addressMargintopdefault;
var addressMargintop = 25;
var address_defaultindex;
var storeValuePrice;
var address_downclick = 0;
var showVideoLoad = 0;
var playNextFlag = 0;
var flagNextPlay = 0;
var storeVisFlag = 0;
/*test variables*/
//var mainUrl = "http://54.158.161.135/";
//var mainUrl = "http://devstore.damedashstudios.com/";

//var videoDataUrl = "gets3Files";