    
/*
     Module Name :
     File Name   :      carousel.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Hari Kumar.
     author      :      Sachin Singh J.
     license     :
     version     :      1.1.1 // written by sachin for present.
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      Getting data for carousels and category div i.e, the thumb image and particular thumb
                        detail.
     Organisation:      Peafowl inc.
*/
//Get my list
function getMyList(){
  var className = "carousels.js";
  var storage = window.localStorage;
  var mylistname = "My List";
     $.ajax({
      type: "POST",
      url: URL_LINK+"/fetchMyList",
      data: JSON.stringify({"fetchMyList":{ "userId": storingIn, "id":storage.getItem(keyDeviceId),"userType":"device"}}),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(data){
       
        var imgData=data.myList;
        if (imgData.length > 0) {
            var HTMLContent = "";
            var MyListCourosalID = mylistname.replace(/ /g, '');
            
            var i;
            var HTMLContent = '<div class="movieCategory_landingPage" id="cate_'+cnt+'"><h5>' + mylistname + '</h5><div id="reg' + cnt + '" class="slidern"> ';
              for (i = 0; i < imgData.length; i++) {
                var stringValue = imgData[i].data.assetName;
                stringValue = stringValue.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                if(i==0){
                  var assetidcnt = imgData[i].data.assetId+'|'+stringValue+'|'+imgData[i].data.description+'|'+imgData[i].data.duration+'|'+imgData[i].data.landscape_url_500x281+'|'+imgData[i].data.releaseDate+'|'+imgData[i].data.director+'|'+imgData[i].data.cast;
                 
                }
             var carodatacontent = imgData[i].data.assetId+'|'+stringValue+'|'+imgData[i].data.description+'|'+imgData[i].data.duration+'|'+imgData[i].data.landscape_url_500x281+'|'+imgData[i].data.releaseDate+'|'+imgData[i].data.director+'|'+imgData[i].data.cast;

              var HTMLContent = HTMLContent + '<div id="regCarousel'+cnt+i+'" title="'+carodatacontent+'"><img  data-lazy="'+imgData[i].data.landscape_url_500x281+'" width="356px" height="199px" /></div>';

            } // for loop close 
            var HTMLContent = HTMLContent +'</div><input type="hidden" name="assetid'+cnt+'" id="assetid'+cnt+'" value="'+assetidcnt+'"><input type="hidden" name="leftid'+cnt+'" id="leftid'+cnt+'" value="0"></div></div>'; 
            //alert(HTMLContent);
            $("#thumbsList").append(HTMLContent);
       
            $('#reg'+cnt).slick({
              lazyLoad: 'ondemand',
                  infinite: true,
                  slidesToShow: 5,
                  slidesToScroll: 1,
                  arrows: false,
                  
            });
          
             cnt++;
           
             if(cnt==1)
             {
               var stringValue = imgData[0].data.assetName;
                stringValue = stringValue.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                 var carodataFirstcontent = imgData[0].data.assetId+'|'+stringValue+'|'+imgData[0].description+'|'+imgData[0].duration+'|'+imgData[0].data.landscape_url_500x281+'|'+imgData[0].data.releaseDate+'|'+imgData[0].data.director+'|'+imgData[0].data.cast;
                  assetDetailsSplit(carodataFirstcontent);
             }
             mylistlength=1;
          }
        },
          error: function(jqXHR, textStatus, errorThrown) {
            var errorLine = "error in line number :24"
            var source = URL_LINK+'/fetchMyList|'+JSON.stringify({"fetchMyList":{ "userId": storingIn, "id":storage.getItem(keyDeviceId),"userType":"device"}})+'|'+className+'|recentwatched()';
            var error = "post call body data is not valid";
            var code = errorLine;
            var details = storage.getItem(email);
            var device = "FireTv";
            onErrorLogInsertionToServer(source,error,code,details,device);
            //onDatabaseInsertion(source,error,code,details,device);
          },
          complete: function() {
            recentwatched();
        
          }                    
    });
}


// Get Recently Watched
function recentwatched(){
  var storage = window.localStorage;
  var urlLink = URL_LINK+"/fetchRecentlyWatched";
   var recentlywatchedname = "Recently Watched";
     $.ajax({
      type: "POST",
      url: URL_LINK+"/fetchRecentlyWatched",
      data: JSON.stringify({"fetchRecentlyWatched":{ "userId": storingIn, "id":storage.getItem(keyDeviceId), "userType":"device"}}),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(data){
        var imgData=data.recentlyWatched;
        if (imgData.length > 0) {
            var HTMLContent = "";
            var RecentCourosalID = recentlywatchedname.replace(/ /g, '');
            
            var i;
            var HTMLContent = '<div class="movieCategory_landingPage" id="cate_'+cnt+'"><h5>' + recentlywatchedname + '</h5><div id="reg' + cnt + '" class="slidern">';
            for (i = 0; i < imgData.length; i++) {
                var stringValue = imgData[i].data.assetName;
                stringValue = stringValue.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
               if(i==0){
                var assetidcnt = imgData[i].data.assetId+'|'+stringValue+'|'+imgData[i].data.description+'|'+imgData[i].data.duration+'|'+imgData[i].data.landscape_url_500x281+'|'+imgData[i].data.releaseDate+'|'+imgData[i].data.director+'|'+imgData[i].data.cast;

                
                }

                var carodatacontent = imgData[i].data.assetId+'|'+stringValue+'|'+imgData[i].data.description+'|'+imgData[i].data.duration+'|'+imgData[i].data.landscape_url_500x281+'|'+imgData[i].data.releaseDate+'|'+imgData[i].data.director+'|'+imgData[i].data.cast;
              

              var HTMLContent = HTMLContent + '<div id="regCarousel'+cnt+i+'"  title="'+carodatacontent+'"><img  data-lazy="'+imgData[i].data.landscape_url_500x281+'" width="356px" height="199px"/></div>';
            } // for loop close 
            var HTMLContent = HTMLContent +'</div><input type="hidden" name="assetid'+cnt+'" id="assetid'+cnt+'" value="'+assetidcnt+'"><input type="hidden" name="leftid'+cnt+'" id="leftid'+cnt+'" value="0"></div></div>'; 
            //alert(HTMLContent);
            $("#thumbsList").append(HTMLContent);
       
             $('#reg'+cnt).slick({
              lazyLoad: 'ondemand',
              infinite: true,
                  slidesToShow: 5,
                  slidesToScroll: 1,
                  arrows: false
            });
            cnt++;
           
            if(cnt==1)
            {
               var stringValue = imgData[0].data.assetName;
                stringValue = stringValue.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                var carodataFirstcontent = imgData[0].data.id+'|'+imgData[0].data.name+'|'+imgData[0].data.description+'|'+imgData[0].data.file_duration+'|'+imgData[0].data.metadata.main_carousel_image_url+'|'+imgData[0].data.metadata.release_date+'|'+imgData[0].data.metadata.director+'|'+imgData[0].data.metadata.cast+'|'+imgData[0].data.tv_show;
                 assetDetailsSplit(carodataFirstcontent);
            }
          recentlength=1;
          }
        },
          error: function(jqXHR, textStatus, errorThrown) {
            var errorLine = "error in line number :98"
            var source =URL_LINK+'/recentwatched|'+JSON.stringify({"fetchRecentlyWatched":{ "userId": storingIn, "id":storage.getItem(keyDeviceId),"userType":"device"}})+'|'+className+'|recentwatched()';
            var error = "post call body data is not valid";
            var code = errorLine;
            var details = storage.getItem(email);
            var device = "FireTv";
            onErrorLogInsertionToServer(source,error,code,details,device);
          },
          complete: function() {
             
           getCarouselNames();
          }                    
    });
}



// Get Carousel Names
function getCarouselNames() {
    //$("#loading").show();
    var storage = window.localStorage;
    var urlLink = URL_LINK+"/getCarousels/"
// alert('getCarousel '+storage.getItem(keyDeviceId));
    $.ajax({
        url: URL_LINK + "/getCarousels/",
        type: "POST",
        data: JSON.stringify({"getCarousels":{"id":"guest_id", "userType":"device"}}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data) {
         
            if (data.length > 0) {
                var i;
                var classactive;
                for (i = 0; i < data.length; i++) {
                    if(i==0){
                         classactive = 'cate_buttonactive';
                    }else{
                        classactive = '';
                    }
                    $("#cateList").append('<li class="'+classactive+'" id="Catedata_' + i + '">' + data[i].carousel_name + '</li>');
                    
                    // $("#searchCat").append('<li  id="searchdata_' + i + '">' + data[i].carousel_name + '</li>');

                    NavItems.push(data[i].carousel_name);
                    CarouselItems.push(data[i].carousel_name);
                    if (data.length == i + 1) {
                      getCarouselItem(NavItems);
                    }
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
         // alert(data);
         // alert('test'+URL_LINK + "/getCarousels/");
         var errorLine = "error in line number :174"
            var source = URL_LINK+'/getCarousels/|'+JSON.stringify({"getCarousels":{"id":storage.getItem(keyDeviceId), "userType":"device"}})+'|'+className+'|getCarouselNames()';
            var error = "post call body data is not valid";
            var code = errorLine;
            var details = storage.getItem(email);
            var device = "FireTv";
            onErrorLogInsertionToServer(source,error,code,details,device);

        },
        complete: function() {
          //  alert(NavItems.length);
         // alert(data);
         // alert('test'+URL_LINK + "/getCarousels/");

        }
    });
}



// Get Get Asset Id With Asset Names 
function getCarouselItem(NavItems) { 
  var storage = window.localStorage;
  var urlLink = URL_LINK+ "/getCarouselData/";
if (NavItems.length > 0) {
        var id = NavItems[0];
        $.ajax({
            url: URL_LINK+ "/getCarouselData/",
            type: "POST",
            data: JSON.stringify({"getCarouselData":{"name": id, "id":"guest_id", "userType":"device"}}),
            contentType: "application/json; charset=utf-8",
            success: function(imgData) { 
             
                if (imgData[id].length > 0) {
                    var HTMLContent = "";
                     var searchData = "";
                    var CourosalID = id.replace(/ /g, '');
                    var i;
                    var HTMLContent = '<div class="movieCategory_landingPage" id="cate_'+cnt+'"><h5>' + id + '</h5><div id="reg' + cnt + '"  class="slidern">';
                  
                    
                    var categorydata = '<div  id="Catecomplete_' + categorycount + '" class="thumbsList hidedivclass"><ul id="Catecomplete_Category' + categorycount + '">';
                    
                   
                    for (i = 0; i < imgData[id].length; i++) {
                      console.log('req data '+imgData[id][i].phone_banner_image_375x667)
                          searchCount++;
                        if( imgData[id][i].phone_banner_image_375x667!=null )
                        {
                           var stringValue = imgData[id][i].assetName;
                           stringValue = stringValue.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                          if(i==0){
                          var assetidcnt = imgData[id][i].assetId+'|'+stringValue+'|'+imgData[id][i].description+'|'+imgData[id][i].duration+'|'+imgData[id][i].landscape_url_500x281+'|'+imgData[id][i].releaseDate+'|'+imgData[id][i].director+'|'+imgData[id][i].cast;
                          }
                           catLstId='CategoryList_'+categorycount+'_'+i;
                           searchLstId='SearchList_'+searchCount;
                           carouselArray.push({"assetId":imgData[id][i].assetId,
                             "assetName":imgData[id][i].assetName,
                             "assetDescription":imgData[id][i].description,
                             "assetFileDuration":imgData[id][i].duration,
                              "assetDirector":imgData[id][i].director,
                              "assetCast":imgData[id][i].cast,
                            "assetImage":imgData[id][i].landscape_url_500x281,
                            "assetImageMain":imgData[id][i].landscape_url_500x281});
                           console.log('JSON data '+imgData[id][i].assetName);
                          
                          var carodatacontent = imgData[id][i].assetId+'|'+stringValue+'|'+imgData[id][i].description+'|'+imgData[id][i].duration+'|'+imgData[id][i].landscape_url_500x281+'|'+imgData[id][i].releaseDate+'|'+imgData[id][i].director+'|'+imgData[id][i].cast;

                          var HTMLContent = HTMLContent + '<div id="regCarousel'+cnt+i+'"  title="'+carodatacontent+'"><img  data-lazy="'+imgData[id][i].landscape_url_500x281+'" width="356px" height="199px" /></div>';

                          /*var categorydata = categorydata + '<li id="'+catLstId+'"  title="'+carodatacontent+'"><h1>"'+imgData[id][i].id+'"<h1/></li>';*/
                          console.log('image '+imgData[id][i].phone_banner_image_375x667);
                          var categorydata = categorydata + '<li id="'+catLstId+'"  title="'+carodatacontent+'"><img  src="'+imgData[id][i].landscape_url_500x281+'" width="346px" height="194px" /></li>';
                          
                        /*   searchData = searchData + '<li   style="display:none" class="vid " id="'+searchLstId+'"  title="'+carodatacontent+'"><img  src="'+imgData[id][i].metadata.movie_art+'"  /></li>';*/
                          

                        }
                          
                    } // for loop close 
                    var HTMLContent = HTMLContent +'</div><input type="hidden" name="assetid'+cnt+'" id="assetid'+cnt+'" value="'+assetidcnt+'"><input type="hidden" name="leftid'+cnt+'" id="leftid'+cnt+'" value="0"></div></div>'; 
                    
                     var categorydata = categorydata +'</ul></div>'; 
                     
                      //alert(HTMLContent);
                    $("#thumbsList").append(HTMLContent);
                    $('#reg'+cnt).slick({
              infinite: true,
                  slidesToShow: 5,
                  slidesToScroll: 1,
                  lazyLoad: 'ondemand',
                  arrows: false
            });
                    $("#CategroyDataDiv").append(categorydata);
                    $("#ulAssetContsearchDivData").append(searchData);
                     cnt++;
                     
                     categorycount++;  
                    if(cnt==1)
                    {
                    var stringValue = imgData[id][0].assetName;
                           stringValue = stringValue.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                       var carodataFirstcontent = imgData[id][0].assetId+'|'+stringValue+'|'+imgData[id][0].description+'|'+imgData[id][0].duration+'|'+imgData[id][0].landscape_url_500x281+'|'+imgData[id][0].releaseDate+'|'+imgData[id][0].director+'|'+imgData[id][0].cast+'|'+imgData[id][0].tv_show;
                       assetDetailsSplit(carodataFirstcontent);
                    }
          }
          
            },
            error: function(jqXHR, textStatus, errorThrown) {
               var errorLine = "error in line number :231" // error may not be this
            var source = URL_LINK+'/getCarouselData/|'+JSON.stringify({"getCarouselData":{"name": id, "id":storage.getItem(keyDeviceId), "userType":"device"}})+'|'+className+'|getCarouselItem()';
            var error = "post call body data is not valid";
            var code = errorLine;
            var details = storage.getItem(email);
            var device = "FireTv";
            onErrorLogInsertionToServer(source,error,code,details,device);
            },
            complete: function() {
                NavItems.splice($.inArray(NavItems[0], NavItems), 1);
                getCarouselItem(NavItems);
                if(NavItems.length == 0){
                  if(STOREVISIBLE == true){
                    getassetStoredata();
                  }
                  else{
                     $(".preloaderContDescribe").fadeOut(); 
                  }
                  //$(".preloaderContDescribe").fadeOut(); 
                  //getassetStoredata();
                }
               
            }
        });
      }
}


function getassetStoredata(){

$.ajax({
    type: "GET",
    url: URL_LINK+"/gets3productData/",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (data) {
       // alert('data '+JSON.stringify(data));
       // videoLinks = data;
      //  HRLURL = videoLinks.dataa[v].videoURL;
        //HRLURL = "https://s3.amazonaws.com/peafowl/puma-store-dev/puma.mp4";
      //  HRLURL = "http://localhost:5000/1.mp4";
       // var HRLPOSTER = videoLinks.dataa[v].posterURL;
       
     var imgData=data.dataa;
        if (imgData.length > 0) {
            var HTMLContent = "";            
            var i;
            var HTMLContent = '<div class="movieCategory_landingPage" id="cate_'+cnt+'"><h5>Stores</h5><div id="reg' + cnt + '" class="slidern">';
            for (i = 0; i < imgData.length; i++) {

             /* if(i==0){
                var assetidcnt = imgData[i].data.id+'|'+imgData[i].data.name+'|'+imgData[i].data.description+'|'+imgData[i].data.file_duration+'|'+imgData[i].data.metadata.main_carousel_image_url+'|'+imgData[i].data.metadata.release_date+'|'+imgData[i].data.metadata.director+'|'+imgData[i].data.metadata.cast;

                
                }

                var carodatacontent = imgData[i].data.id+'|'+imgData[i].data.name+'|'+imgData[i].data.description+'|'+imgData[i].data.file_duration+'|'+imgData[i].data.metadata.main_carousel_image_url+'|'+imgData[i].data.metadata.release_date+'|'+imgData[i].data.metadata.director+'|'+imgData[i].data.metadata.cast;*/
              

              var HTMLContent = HTMLContent + '<div id="reg'+cnt+i+'" ><img  data-lazy="'+imgData[i].posterURL+'" width="356px" height="199px"/></div>';
            } // for loop close 
            var HTMLContent = HTMLContent +'</div><input type="hidden" name="assetid'+cnt+'" id="assetid'+cnt+'" ><input type="hidden" name="leftid'+cnt+'" id="leftid'+cnt+'" value="0"></div></div>'; 
            //alert(HTMLContent);
            $("#thumbsList").append(HTMLContent);
       
             $('#reg'+cnt).slick({
              lazyLoad: 'ondemand',
              infinite: true,
                  slidesToShow: 5,
                  slidesToScroll: 1,
                  arrows: false
            });
            cnt++;
           
          /*  if(cnt==1)
            {
                var carodataFirstcontent = imgData[0].data.id+'|'+imgData[0].data.name+'|'+imgData[0].data.description+'|'+imgData[0].data.file_duration+'|'+imgData[0].data.metadata.main_carousel_image_url+'|'+imgData[0].data.metadata.release_date+'|'+imgData[0].data.metadata.director+'|'+imgData[0].data.metadata.cast+'|'+imgData[0].data.tv_show;
                 assetDetailsSplit(carodataFirstcontent);
            }
          recentlength=1;*/
          storelength =1;
          }
    },
    error: function (jqXHR, textStatus, errorThrown) {
        // alert("  ");
        console.log("error ---");
    },
     complete: function() {
               
                  $(".preloaderContDescribe").fadeOut(); 
                
                }
               
         
});
}





