/*
     Module Name :
     File Name   :      createRecentWatch.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      1.0.0 // written by sachin for present.
     Created on  :      8 august 12:26 pm
     Last modified on:  30th November
     Description :      function for creating reccently watched list.
     Organisation:      Peafowl inc.
*/
function createRecentlyWatchedList(completeData,assetId)
{

  flagAddList=1;
   //$(document).ready(function() 
   //   {
                $.ajax({
                        type: "POST",
                        url: URL_LINK +"/createRecentlyWatched",
                        // The key needs to match your method's input parameter (case-sensitive).
                        data: JSON.stringify({"createRecentlyWatched":{"videoId":assetId,"userId":storingIn}}),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data){
                         
                        },
                        failure: function(errMsg) {
                           // alert(errMsg);
                        }
                    });
          //    });
}