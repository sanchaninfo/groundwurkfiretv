/*
     Module Name :
     File Name   :      rightClick.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      0.0.12 
     Created on  :      9th october 
     Last modified on:  30th November
     Description :      right click functionality for fire tv remote control
     Organisation:      Peafowl inc.
*/
function rightClickFunction() {
    var isThumbListVisibleTrue = $('#thumbsList').is(':visible');
    var ismenuDiv = $('#menuDiv').is(':visible');
    var catVisible = $('#Catecomplete_' + countDownClickVar).is(':visible');
    var episodesDivVisible = $('#episodeMainDiv').is(':visible');
    var catSelectVisible = $('#categorySelectMain').is(':visible');
    var assetCountValue = categoryValueCount + categoryDownClick;
    var assetDataValues = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
    var searchDivVisible = $('#divSearch').is(':visible');
    var searchContVisible = $('.vid').is(':visible');
    var videoRedirectIsVisible = $('#videoRedirect').is(':visible');
    var slideVisible = $('#slideShowImg').is(':visible');
    var slideShowVisible = $('#landingPageCarSlide').is(':visible');
    var slideShowMainVisible = $('#MainDivSlide').is(':visible');
    var productsStoreVisible = $('#ProductDetails').is(':visible');
    if(slideShowVisible && slideShowMainVisible){
     
    }
    else if(storeSliderVisible == 1){
     /* if($("#slider").hasClass("homeactive") && $("#showprod").css("visibility") == "visible"){
         alert('testing val');
                $(".slider"+v+carousel_value).addClass('snext');
               var $carousel = $('.snext');
               $carousel.slick('slickNext');
               $(".slider-for"+v+carousel_value).addClass('snext');
               var $carousel = $('.snext');
               $carousel.slick('slickNext');
            }
            else if(productsStoreVisible){
              alert('test product rigt');
            }*/
            
      rightClick();
    }
    else if (isThumbListVisibleTrue) {
        //$.fn.als("next", countDownClickVarLanding);
        
        $("#reg"+countDownClickVarLanding).addClass('snext');

       // $("#reg"+countDownClickVarLanding).slick('unslick');
       if(slickVal == countDownClickVarLanding){
        
        changeSlick = 1;
         var $carousel = $('.snext');
        $carousel.slick('slickNext');
       }
       
       
         
    }
else if(slideShower == 1){
   
 $("#reg_Slide").addClass('snext');

       // $("#reg"+countDownClickVarLanding).slick('unslick');
       if(slickVal == countDownClickVarLanding){
        
        changeSlick = 1;
         var $carousel = $('.snext');
        $carousel.slick('slickNext');
       }

}
     else if (ismenuDiv) {
        //categoryValueCount++;
        //categoryDownClick++;
       // $("#menuDiv").fadeOut();
        // var div = $("div");
        //$("#CategroyDataDiv").animate({height: '300px', opacity: '0.4'}, "slow");
        console.log('countDownClickVar ' + countDownClickVar);
        catListCount = $("#Catecomplete_Category"+countDownClickVar+" li").length;
       // alert("cat length "+$("#Catecomplete_Category"+countDownClickVar+" li").length);
        //$("#Catecomplete_Category"+countDownClickVar+" li").length
        $("#menuDiv").animate({
            left:"-25%"   
        }, 500,  function() {
             $("#image1").fadeIn("slow");
        $("#descTxtCat").fadeIn();
        }).hide();

        $("#CategroyDataDiv").animate({
            right:"0px"   
        }, 500);

        $("#Catecomplete_" + countDownClickVar).animate({
            marginLeft:"116px"
        }, 500);


       
        assetDetailsSplitCat(assetDataValues);
        //$('#catBackgroundImage').css("background-image", "url("+assetDataArray[4]+")"); 
        categoryVisibleChange = true; 
        
    } else if (catVisible) {
        
        categoryValueCount++;
        console.log('value count right '+categoryValueCount)
        categoryDownClick++;


        var assetVal = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
        if (categoryValueCount > 4 || assetVal == null) {
            categoryDownClick--;
            categoryValueCount--;
            flagRightClickCat = 1;
        }
        if (assetVal != null && flagRightClickCat == 0) {
            if (categoryValueCount <= 4) {
                //assetDetailsSplitCat(assetVal);
               clearTimeout(timerCategory);  
                timerCategory = setTimeout(function() { 
                     //var assetValues = $('#assetid' + countDownClickVarLanding).val();
                     //assetDetailsSplitCat(assetData);
                      assetDetailsSplitCat(assetVal);
                }, 1000);

                // $('#catBackgroundImage').css("background-image", "url("+assetDataArray[4]+")"); 
                rightMov = marginValue + rightMov;
                // $("#image1").css({ marginLeft : rightMov+"px"});
                $("#image1").animate({
                    marginLeft: rightMov + "px"
                }, 100);
            }
        } else {
            flagRightClickCat = 0;
            rightAssetFound = 1;
        }

    } else if (episodesDivVisible) {

        $("#episodeDescription").addClass("episodeInactive");
        $(".borderEpisodes").fadeIn();
        $("#episodeList").removeClass("episodeInactive");
    } else if (catSelectVisible) {
        var divCount = $(".icon_sel").length;
        //alert('catSelectCount '+catSelectCount);
            /*  if(catSelectCount<$('ul#menu li').length){
                  $("#"+catSelectCount).removeClass("categoriesDiv_active");
                  catSelectCount++;
                  $("#"+catSelectCount).addClass("categoriesDiv_active")
              }*/
        if (catSelectCount < divCount) {
            $("#" + catSelectCount).removeClass("active_sel");
            catSelectCount++;
            $("#" + catSelectCount).addClass("active_sel")
        }
        if (catSelectCount == 1) {
            $("#txtDescribe").html("Search for TV shows, movies, categories and music");
        } else if (catSelectCount == 2) {
            $("#txtDescribe").html("Browse categories");
        } else if (catSelectCount == 3) {
            $("#txtDescribe").html("Account settings");
        } else if (catSelectCount == 4) {
            $("#txtDescribe").html("Exit Groundwurk");
        }
    } else if (searchDivVisible) {
        var searchBorder = $('#divimageBorderSearch').is(':visible');
        if (keyPadCount < 38) {

            if (keyPadCount == 2 && countDownKeyPad == 0 ) {
                //countDownKeyPad++;
              /*   alert('test1')
                if (searchContVisible == true) {
                   // $("#" + keyPadCount + "g").removeClass("searchContBackActive");
                    $("#divimageBorderSearch").show();
                }*/
            } else {
               
                if (keyPadCount == keyPadValue) {
                   
                    if (searchContVisible) {
                     
                        if(keyPadCount == 2){
                         
                        $("#" + keyPadCount + "g").removeClass("searchContBackActive");
                         $("#" + keyPadCount + "g").css({
                        color: 'white'
                            });
                        }
                        else{
                        
                              $("#" + keyPadCount + "g").removeClass("searchCont_active");
                         $("#" + keyPadCount + "g").css({
                        color: 'white'
                    });
                        }
                      
                        searchCountAsset++;
                        
                        var assetValSearch = $("#SearchList_" + searchCountAsset).attr("title");
                        if (assetValSearch != null) {
                           
                            if (searchCountAsset == 1) {
                                
                                $("#divimageBorderSearch").show();
                                
                                var assetValSearch = $("#SearchList_" + searchCountAsset).attr("title");
                            } else if (searchRightClick < 2) {
                                
                                $("#divimageBorderSearch").show();
                                searchRightClick++;
                                var assetValSearch = $("#SearchList_" + searchCountAsset).attr("title");
                                
                                rightMovSearch = marginValueSearch + rightMovSearch;
                                $("#divimageBorderSearch").animate({
                                    marginLeft: rightMovSearch + "px"
                                }, 50);
                            } else {

                                searchCountAsset--;
                                
                            }
                        }
                    }
                } else {
                    if(keyPadCount == 1 ){
                       
                         $("#" + keyPadCount + "g").removeClass("searchCont_active_space");
                        $("#" + keyPadCount + "g").css({
                        color: 'white'
                        });
                        keyPadCount++;
                        $("#" + keyPadCount + "g").addClass("searchContBackActive");
                        $("#" + keyPadCount + "g").css({
                            color: 'black'
                        });
                    }
                    else{
                         $("#" + keyPadCount + "g").removeClass("searchCont_active");
                    $("#" + keyPadCount + "g").css({
                        color: 'white'
                    });
                    keyPadCount++;
                    $("#" + keyPadCount + "g").addClass("searchCont_active");
                    $("#" + keyPadCount + "g").css({
                        color: 'black'
                    });
                    }
                   
                    var assetVal = $("#" + keyPadCount + "g").attr("title");
                }


            }


        }
    }
    else if(videoRedirectIsVisible){

        var divs = $('#videFunc1').find('div').length;
       // alert('divs '+divs);

       if($( "#videFunc0" ).hasClass( "videFuncActive" )){

       }
       else if($( "#videFunc1" ).hasClass( "videFuncActive" )){
       
          if(nxtVidFunc < divs-1){  
               
             $("#child" + nxtVidFunc).removeClass("VEplaybtn_active");
            nxtVidFunc++;
            $("#child" + nxtVidFunc).addClass("VEplaybtn_active")
        }
       }       
    }
}