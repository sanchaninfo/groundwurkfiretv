/*
     Module Name :
     File Name   :      upClick.js
     Project     :      damedashstudios
     Copyright (c)      Damedash Studios.
     author      :      Sachin Singh J.
     author      :
     license     :
     version     :      0.0.12 
     Created on  :      9th october 
     Last modified on:  30th November
     Description :      upclick functionality for fire tv remote control
     Organisation:      Peafowl inc.
*/
function upClickFunction() {
    isDescriptionDivVisible = $('#descriptionDiv').is(':visible');
    isThumbsListVisible = $('#thumbsList').is(':visible');
    var isEpisode3Visible = $('#episode_action3').is(':visible');
    var isButtonShow = $('#buttonShow').is(':visible');
    var isEpisodeThumbsVisible = $('#episodeButtons').is(':visible');
    var isEpisodeResumeVisible = $('#episode_actionResume').is(':visible');
    var isdownmenuDiv = $('#menuDiv').is(':visible');
    var catVisible = $('#Catecomplete_' + countDownClickVar).is(':visible');
    var searchDivVisible = $('#divSearch').is(':visible');
    var searchBorder = $('#divimageBorderSearch').is(':visible');
     var settingsDiv = $('#divSettingsMain').is(':visible');
     var videoRedirectIsVisible = $('#videoRedirect').is(':visible');
    if (isEpisodeThumbsVisible) {
       
        //$.fn.als("prev",cnt);
        if ($("#episodeDescription").hasClass("episodeInactive")) {
            episodeAssetDownClick--;
            var assetData = $("#Episode_" + episodeDiv + "_" + episodeAssetDownClick).attr("title");
            if (assetData != null) {
                episodetopMov = episodetopMov - topmovdownclickEpisode;
                $("#episodeSeason_" + episodeDiv).animate({
                    marginTop: -episodetopMov + "px"
                }, 500);
            } else {
                episodeAssetDownClick++;
            }
        } else {
            if (episodeDiv == 0) {
                episodeDiv = 0;
            } else {

                $("#season_" + episodeDiv).removeClass('catActive');
                //$("#Catecomplete_"+countDownClickVar).fadeOut();
                $("#episodeSeason_" + episodeDiv).hide();
                episodeDiv--;
                $("#season_" + episodeDiv).addClass('catActive');
                $("#episodeSeason_" + episodeDiv).show();

            }
        }
    }else if(storeSliderVisible == 1){
        upClick();
    }
     else if (isDescriptionDivVisible && isButtonShow) {
      
        descriptionUpClick(isEpisode3Visible, isEpisodeResumeVisible, resumeEpisodeStat)
    } else if (isThumbsListVisible && mainDivFlag == 1) {
      
        $("#reg"+countDownClickVarLanding).removeClass("snext");
        //$(".slidern").removeClass("snext");
        if (countDownClickVarLanding == 0) {
            countDownClickVarLanding = 0;
            slickVal = 0;
        } else {
            countDownClickVarLanding--;
            slickVal--;
            
            $("#cate_" + countDownClickVarLanding).animate({
                marginTop: '0px',
               
            }, 600,function() {
//$("#assetid"+countDownClickVarLanding).val($("#reg" + countDownClickVarLanding +slideShowVal).attr("title"));
                clearTimeout(timer);  
                timer = setTimeout(function() { 

                     var assetValues = $('#assetid' + countDownClickVarLanding).val();
                      console.log('assetValue '+countDownClickVarLanding)
                     assetDetailsSplit(assetValues);
                }, 1000);
                
             });



      /* setTimeout(function() {
        var assetValues = $('#assetid' + countDownClickVarLanding).val();
        //var assetValues =  $("#reg" + countDownClickVarLanding +slideShowVal).attr("title")
        assetDetailsSplit(assetValues);
        }, 5000);*/


            
            
        }
       
    } else if (isdownmenuDiv) {
     
        if (countDownClickVar == 0) {
            countDownClickVar = 0;
        } else {

            $("#Catedata_" + countDownClickVar).removeClass('cate_buttonactive');
            //$("#Catecomplete_"+countDownClickVar).fadeOut();
            $("#Catecomplete_" + countDownClickVar).hide();
            countDownClickVar--;
            var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
            assetDetailsSplitCat(assetData);
            $("#Catedata_" + countDownClickVar).addClass('cate_buttonactive');
            $("#Catecomplete_" + countDownClickVar).show();

        }
    } else if (catVisible) {
       
        upClickCat++;
        catListCount = $("#Catecomplete_Category"+countDownClickVar+" li").length;
        categoryDownClick = categoryDownClick - 5;
        var assetData = $("#CategoryList_" + countDownClickVar + "_" + categoryDownClick).attr("title");
        if (assetData != null) {
            topMov = topMov - topmovdownclick;

$("#Catecomplete_" + countDownClickVar).animate({
                marginTop: -topMov + "px"
            }, 600,function() {
               

                clearTimeout(timerCategory);  
                timerCategory = setTimeout(function() { 
                     //var assetValues = $('#assetid' + countDownClickVarLanding).val();
                     assetDetailsSplitCat(assetData);
                }, 2000);

             });


           // assetDetailsSplitCat(assetData);
            $("#Catecomplete_" + countDownClickVar).animate({
                marginTop: -topMov + "px"
            }, 500);
        } else {
            categoryDownClick = categoryDownClick + 5;
        }
    } else if (searchDivVisible) {
        
         if (searchBorder) {
            searchCountAsset = searchCountAsset - 3;
            var assetValSearch = $("#SearchList_" + searchCountAsset).attr("title");
            if (assetValSearch != null) {
                topMovSearch = topMovSearch - topMovSearchDim;
                $("#divsearchAssetCont").animate({
                    marginTop: -topMovSearch + "px"
                }, 500);
            } else {
                searchCountAsset = searchCountAsset + 3;
            }
        } else {
            if (countDownKeyPad > 0) {

                countDownKeyPad--;
                if (keyPadValue > 3) {
                    if (countDownKeyPad == 0) {
                        
                        keyPadValue = 2;
                        $("#" + keyPadCount + "g").removeClass("searchCont_active");
                        $("#" + keyPadCount + "g").css({
                            color: 'white'
                        });
                        keyPadCount = 1;
                        $("#" + keyPadCount + "g").addClass("searchCont_active_space");
                        $("#" + keyPadCount + "g").css({
                            color: 'black'
                        });
                    } else {

                        leftClickSearchFun = leftClickSearchFun - 6;

                        keyPadValue = keyPadValue - 6;

                        $("#" + keyPadCount + "g").removeClass("searchCont_active");
                        $("#" + keyPadCount + "g").css({
                            color: 'white'
                        });
                        keyPadCount = keyPadCount - 6;
                        $("#" + keyPadCount + "g").addClass("searchCont_active");
                        $("#" + keyPadCount + "g").css({
                            color: 'black'
                        });
                    }

                }

            }
            else{
                if(keyPadCount == 1){
                     $("#" + keyPadCount + "g").removeClass("searchCont_active_space");
                        $("#" + keyPadCount + "g").css({
                            color: 'white'
                        });
                }
                else if(keyPadCount == 2){
                    $("#" + keyPadCount + "g").removeClass("searchContBackActive");
                        $("#" + keyPadCount + "g").css({
                            color: 'white'
                        });
                }
                
                $("#divKeyBoardBackBtn").addClass("searchCont_active");
                $("#divKeyBoardBackBtn").css({
                            color: 'black'
                        });
            }
        }

    }else if(settingsDiv){
       
        if(settingsClick > 0){
                $("#divSettingsMainExitBtn").removeClass("src_nav_active");
                settingsClick --;
                $("#divSettingsMainBackBtn").addClass("src_nav_active"); 
                $("#gbBackBrowse").addClass("gb_act");              
        }
    }
      else if(videoRedirectIsVisible){
        
     if(videoRedirectActive>0){
        
         $("#videFunc" + videoRedirectActive).removeClass("videFuncActive");
            videoRedirectActive--;
        $("#videFunc" + videoRedirectActive).addClass("videFuncActive"); 
         $("#backVidRet").addClass("VEbackbtn_act"); 
          
     }   
    }
    else if(slideShower == 1){
      
    }
}